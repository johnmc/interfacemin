# Installation
```
cd Bin
cmake -DCMAKE_Fortran_COMPILER=gfortran -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++  ..
make
make install

```
The executable is `test`.

The executable should be passed a string to represent the process being computed.
A simple benchmark test can be performed, checking the results of the calculation
against the known values at a single phase space point.  It is enabled using the
`-b` flag.  Currently implemented cases are:
```
./test -b u d~ ve e+
./test -b u d~ ve e+ g
./test -b u d~ ve e+ g g
./test -b u u~ e- e+
./test -b u u~ e- e+ g
./test -b u u~ e- e+ g g
./test -b g g g g g
```
Sample output:
```
-bash-4.2$ ./test -b g g g g g
*************************************************************
*                                                           *
*  MCFM Interface running MCFM v10.1                        *
*                                                           *
*  Please cite:                                             *
*  J.M. Campbell, R.K. Ellis              hep-ph/9905386    *
*  J.M. Campbell, R.K. Ellis, C. Williams arXiv:1105.0020   *
*  J.M. Campbell, T. Neumann              arXiv:1909.09117  *
*  J.M. Campbell, S. Hoeche, C.T. Preuss  arXiv:2107.04472  *
*                                                           *
*************************************************************

 ************** Electroweak parameters **************
 *                                                  *
 *  zmass(+)   91.1876000     wmass(+)  80.3850000  *
 * zwidth(+)    2.4952000    wwidth(+)   2.0850000  *
 *     Gf(+) 0.119745D-04   1/aemmz    128.7396108  *
 *     xw       0.2229303      mtop(+) 173.2100000  *
 *   gwsq       0.4378533       esq      0.0976108  *
 * top width    1.0000000 (NLO)                     *
 *    mb        0.0000000        mc      0.0000000  *
 *                                                  *
 * Parameters marked (+) are input, others derived  *
 ****************************************************

 Generating 1 flat phase space points

 Finite: MCFM = 0.030909089219912787
 IR:     MCFM = 0.0033726284546849276
 IR2:    MCFM = -0.037214918617662747
 Born:   MCFM = 0.13210634455727832

 Finite: ratio = 1
 IR:     ratio = 1
 IR2:    ratio = 1
 Born:   ratio = 1

Benchmark with tolerance: 9.99999999999999979e-17
 PASSED  

*************************************************************
*  Thanks for using MCFM.                                   *
*                                                           *
*  Please cite:                                             *
*  J.M. Campbell, R.K. Ellis              hep-ph/9905386    *
*  J.M. Campbell, R.K. Ellis, C. Williams arXiv:1105.0020   *
*  J.M. Campbell, T. Neumann              arXiv:1909.09117  *
*  J.M. Campbell, S. Hoeche, C.T. Preuss  arXiv:2107.04472  *
*                                                           *
*************************************************************
```

# Additional details

Working simple processes are:
```
./test u u~ e- e+
./test u d~ ve e+
./test g g h
./test -Pmodel=heft g g h
./test u d~ ve e+ g
./test u u~ e- e+ g
```
More complex processes are:
```
./test u d~ ve e+ g g
./test u d~ ve e+ u u~ 
./test u d~ ve e+ d d~
./test u d~ ve e+ s s~
```
```
./test u u~ e- e+ g g
./test u u~ e- e+ u u~ 
./test u u~ e- e+ d d~ 
./test u u~ e- e+ c c~ 
```
```
./test u u~ d d~ g
./test u u~ u u~ g
./test u u~ g g g
```
```
./test g g g g g
```
```
./test g g h g g
```

The default behavior is to perform the calculation of the matrix element at 10 phase-space points.
To evaulate more points, and increase the execution time of the code, use the `-n<points>` argument,
e.g.
```
./test -n1000 u d~ ve e+ g
```

In the basic installation checks against the external libraries OpenLoops and Recola are not performed.
Installation with checks requires:
```
cd Bin
cmake -Dwith_library=ON -DCMAKE_Fortran_COMPILER=mpifort -DCMAKE_C_COMPILER=mpicc -DCMAKE_CXX_COMPILER=mpic++ -DOLDIR=/path/to/OpenLoops -DRCLDIR=/path/to/recola2-2.2.3 ..
make
make install
```
The OpenLoops and Recola libraries must be installed separately.
This produces additional executables `oltest` and `rcltest`.

# Code background

The code is part of the MCFM package: https://mcfm.fnal.gov/

Main MCFM authors:
 - John Campbell <johnmc@fnal.gov>
 - Keith Ellis <ellis@fnal.gov>
 - Tobias Neumann <tneumann@fnal.gov>
 - Ciaran Williams <ciaranwi@buffalo.edu>

See https://mcfm.fnal.gov/doc/contributors.html for a full list of
contributors, a full list of processes and process references.


# References

Papers:

 - Precision phenomenology with MCFM (MCFM 9.0)
   J. M. Campbell and T. Neumann, arXiv:1909.09117

 - An update on vector boson pair production at hadron colliders
   J.M. Campbell, R.K. Ellis, arXiv:hep-ph/9905386

 - Vector boson pair production at the LHC
   J. M. Campbell, R. K. Ellis and C. Williams, arXiv:1105.0020

 - Fiducial qT resummation of color-singlet processes at N3LL+NNLO
   Thomas Becher, Tobias Neumann, arXiv:2009.11437

MCFM uses the following libraries:

  - amos - a package for Bessel functions of complex argument,
        http://www.netlib.org/amos/
  - QCDLoop 2.0.8 (S. Carrazza, R.K. Ellis, G. Zanderighi)
  - Quad Double 2.3.22 (Y. Hida, X.S. Li, D.H. Bailey)
  - LHAPDF 6.2.3 (A. Buckley et al.)
  - handyG 0.1.4 (L. Naterop, A. Signer, Y. Ulrich)
  - hplog6 1.6 (T. Gehrmann, E. Remiddi)
  - OneLOop (A. van Hameren)

