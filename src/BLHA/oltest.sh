#!/bin/bash

target=$1
test -z "$target" && target=6
echo "Target accuracy: "$target" digits"

function do_test(){
  if (( $target == 0 )); then
    res=$(./oltest -vpn1 $* | grep MCFM\ = | sed 's/MCFM.*Ratio =//1;s/Finite:/F =/1;s/IR:/IR =/;s/IR2:/IR2 =/;s/Born:/B =/')
  else
    res=$(./oltest -vpn1 $* | grep MCFM\ = | sed 's/MCFM.*Ratio =//1;s/Finite:/F =/1;s/IR:/IR =/;s/IR2:/IR2 =/;s/Born:/B =/' |\
    awk '{for(i=1;i<=NF;i+=1){a[2]=a[1];a[1]=a[0];a[0]=$i;if(a[1]=="="){\
          dev=a[0]-1;if(dev>-1e-'$target'&&dev<1e-'$target')print a[2]" \033[32mok\033[0m";else print "\033[31mfailed\033[0m";}}}')
  fi
  echo $*": "$res
}

function test_z() {
echo -e "\033[1m===> Z processes <===\033[0m"
do_test d d~ e- e+
do_test d~ d e- e+
do_test u u~ e- e+
do_test u~ u e- e+
}
function test_wm() {
echo -e "\033[1m===> W- processes <===\033[0m"
do_test d u~ e- ve~
do_test u~ d e- ve~
}
function test_wp() {
echo -e "\033[1m===> W+ processes <===\033[0m"
do_test d~ u e+ ve
do_test u d~ e+ ve
}
function test_heft() {
echo -e "\033[1m===> h (EFT) processes <===\033[0m"
do_test g g h -Pmodel=heft
}
function test_hsm() {
echo -e "\033[1m===> h (SM) processes <===\033[0m"
do_test g g h
}

test_z
test_wm
test_wp
test_heft
test_hsm
