// Copyright (C) 2021 John Campbell, Stefan Hoeche, Christian T Preuss.
//
//  This program is free software: you can redistribute it and/or modify it under
//  the terms of the GNU General Public License as published by the Free Software
//  Foundation, either version 3 of the License, or (at your option) any later
//  version.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT ANY
//  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
//  PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License along with
//  this program. If not, see <http://www.gnu.org/licenses/>
 
// Program to test MCFM interface against OpenLoops.

// Standard includes.
#include <ctime>
#include <fstream>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

// MCFM includes
#include "MCFM/CXX_Interface.h"
#include "params.cxx"
#include "Rambo.cxx"

void set_sm_parameters_ol(std::map<std::string,std::string> &params,
			  const int diagonalCKM=1);

using namespace MCFM;

int convert_pid(const char* in) {
  if (strcmp("d~",in)==0) return -1;
  if (strcmp("d",in)==0) return 1;
  if (strcmp("u~",in)==0) return -2;
  if (strcmp("u",in)==0) return 2;
  if (strcmp("s~",in)==0) return -3;
  if (strcmp("s",in)==0) return 3;
  if (strcmp("c~",in)==0) return -4;
  if (strcmp("c",in)==0) return 4;
  if (strcmp("b~",in)==0) return -5;
  if (strcmp("b",in)==0) return 5;
  if (strcmp("t~",in)==0) return -6;
  if (strcmp("t",in)==0) return 6;
  if (strcmp("e+",in)==0) return -11;
  if (strcmp("e-",in)==0) return 11;
  if (strcmp("mu+",in)==0) return -13;
  if (strcmp("mu-",in)==0) return 13;
  if (strcmp("tau+",in)==0) return -15;
  if (strcmp("tau-",in)==0) return 15;
  if (strcmp("ve~",in)==0) return -12;
  if (strcmp("ve",in)==0) return 12;
  if (strcmp("vmu~",in)==0) return -14;
  if (strcmp("vmu",in)==0) return 14;
  if (strcmp("vtau~",in)==0) return -16;
  if (strcmp("vtau",in)==0) return 16;
  if (strcmp("h",in)==0) return 25;
  if (strcmp("g",in)==0) return 21;
  if (strcmp("a",in)==0) return 22;
  if (strcmp("Z",in)==0) return 23;
  if (strcmp("W+",in)==0) return 24;
  if (strcmp("W-",in)==0) return -24;
  return 0;
}

// Main program.
int main(int argc, char** argv) {

  double Ecm = 350., mcfm_nptsfac=1.;
  int foEW = -1, foQCD = -1, diagonalCKM = 1;
  int nPts = 10, checkPoles = 0, benchmark = 0;
  int printMoms = 0, ampType = 11, fampType = -1;
  std::vector<int> decids, decfls;

  // Collect settings.
  std::map<std::string,std::string> params;
  read_parameters("params.lh",params);

  int opt, verbosity = 0;
  while ((opt = getopt(argc, argv, "pvbmA:n:e:o:S:W:CD:P:h")) != -1) {
    switch (opt) {
    case 'p': checkPoles = 1; break;
    case 'v': verbosity += 1; break;
    case 'b': {checkPoles = 1; benchmark = 1; nPts =1; break; }
    case 'm': printMoms = 1; break;
    case 'n': nPts = atoi(optarg); break;
    case 'e': mcfm_nptsfac = atoi(optarg); break;
    case 'A': fampType = atoi(optarg); break;
    case 'W': foEW = atoi(optarg); break;
    case 'S': foQCD = atoi(optarg); break;
    case 'C': diagonalCKM = 0; break;
    case 'P': {
      size_t pos(0);
      std::string arg(optarg);
      for (;pos<arg.length();++pos) if (arg[pos]=='=') break;
      params[arg.substr(0,pos)]=arg.substr(pos+1,std::string::npos);
      break;
    }
    case 'D': {
      size_t pos(0);
      std::string arg(optarg);
      for (;pos<arg.length();++pos)
	if (arg[pos]==',') break;
      decids.push_back(atoi(arg.substr(0,pos).c_str()));
      decfls.push_back(convert_pid(arg.substr(pos+1,std::string::npos).c_str()));
      break;
    }
    case 'h':
      std::cout<<argv[0]<<": Command line options\n"
	       <<"  -h        -- print this help message\n"
	       <<"  -v        -- increase verbosity\n"
	       <<"  -b        -- perform benchmark test\n"
	       <<"  -p        -- print ME values and enable pole check\n"
	       <<"  -m        -- print momentum configuration\n"
	       <<"  -C        -- enable non-diagonal CKM\n"
	       <<"  -n points -- set number of phase space points to 'points'\n"
	       <<"  -e factor -- set factor for MCFM extra points to 'points'\n"
	       <<"  -S oQCD   -- set QCD order to 'oQCD'\n"
	       <<"  -W oEW    -- set EW order to 'oEW'\n"
	       <<"  -A type   -- set OpenLoops amplitude type to 'type'\n"
	       <<"  -P p=val  -- set parameter 'p' to 'val'\n"
	       <<"  -D id,fl  -- add decay id and flavor 'id' and 'fl'\n";
      exit(0);							
    default:
      std::cerr<<"Usage: "<<argv[0]<<" [-vpmC] [-n #pts] [-S oQCD] [-W oEW] [-A type] [-P p,val] [-D id,fl] [process]"<<std::endl;
      exit(EXIT_FAILURE);
    }
  }
  if (optind >= argc) {
    std::cerr<<"You did not specify a process"<<std::endl;
    exit(EXIT_FAILURE);
  }

  //---------------------------------------------------------------------------
  // Process and initialization.

  // Process.
  std::vector<int> ids;
  int oEW = 0, oQCD = 1, nh = 0; // Needed for OpenLoops.
  for (int i(optind);i<argc;++i) {
    ids.push_back(convert_pid(argv[i]));
    if (ids.back()==21 || (abs(ids.back()))>0 && abs(ids.back())<7) ++oQCD;
    else {
      if (ids.back()==25) ++nh;
      ++oEW;
    }
  }
  // Note: in general, this does not capture all cases.
  oQCD-=2;

  int nIn = 2, nOut = ids.size()-nIn;

  //---------------------------------------------------------------------------
  // MCFM.

  // Create instance of MCFM interface.
  CXX_Interface mcfm;

  if (nh && oEW == nh) {
    params["H_width"] = "0";
    if (params["model"] != "heft") {
      ampType = 12;
      oQCD -= 1;
    }
    oQCD += 2;
  }
  if (foEW>=0) oEW=foEW;
  if (foQCD>=0) oQCD=foQCD;
  if (fampType>0) ampType=fampType;

  // Initialize MCFM.
  mcfm.Initialize(params);
  mcfm.SetVerbose(verbosity);

  // Print settings.
  if (verbosity) mcfm.PrintSettings();

  // Initialize process.
  Process_Info pi(ids,2,oQCD,oEW);
  pi.m_decids=decids;
  pi.m_decfls=decfls;
  pi.m_model=params["model"];
  int pid_mcfm(mcfm.InitializeProcess(pi));
  if (pid_mcfm < 0) {
    std::cout << " Process not available in MCFM." << std::endl;
    exit(EXIT_FAILURE);
  }

  // Enable calculation of pole tems.
  mcfm.SetPoleCheck(checkPoles);

  // Initialize process.
  std::string process;
  for (int i(0);i<nIn;++i) process += std::to_string(ids[i])+" ";
  process += "->";
  for (int i(nIn);i<nIn+nOut;++i) process += " "+std::to_string(ids[i]);
  
  //---------------------------------------------------------------------------
  // Test on phase space points generated with RAMBO.

  // Compare CPU time.
  clock_t t;
  double  cput_mcfm;
  double  cput_ps;
  double  benchres[4];
  double  benchtoler = 1e-16;
  int  passedbench = 1;

  std::cout << "\n Generating " << nPts << " flat phase space points\n\n";
  if (checkPoles) {
    for (int iPt(0);iPt<nPts;++iPt) {
      // Generate phase space point with OpenLoops.
      std::vector<FourVec> pn;
      GenPoint(Ecm, pn, ids, nIn, params);

      if (benchmark) {
        mcfm.GetBench(pid_mcfm, iPt, pn, benchres);
      }

//      std::cout << std::setprecision(17) << pn[0] << std::endl;
//      std::cout << std::setprecision(17) << pn[1] << std::endl;
//      std::cout << std::setprecision(17) << pn[2] << std::endl;
//      std::cout << std::setprecision(17) << pn[3] << std::endl;
//      std::cout << std::setprecision(17) << pn[4] << std::endl;
//      std::cout << std::setprecision(17) << pn[5] << std::endl << std::endl;

      // Calculate result with MCFM.
      mcfm.SetAlphaS(std::stod(params["alpha_S"]));
      t = clock();
      mcfm.Calc(pid_mcfm, pn, 1);
      t = clock() - t;
      cput_mcfm += double(t)/CLOCKS_PER_SEC;
      std::vector<double> res_mcfm = mcfm.GetResult(pid_mcfm);
      // Translate MCFM result to same normalization and scheme conventions as OpenLoops.
      for (int i(0);i<4;++i)
        res_mcfm[i] /= mcfm.GetProcess(pid_mcfm)->GetSymmetryFactor();
      res_mcfm[0] -= res_mcfm[3]*mcfm.GetProcess(pid_mcfm)->GetSchemeFactor(0);
      
      // Print result and optionally phase space point.
      if (ampType == 11) {
        std::cout << " Finite: MCFM = " << std::setprecision(17) << res_mcfm[0] << std::endl
                  << " IR:     MCFM = " << res_mcfm[1] << std::endl
                  << " IR2:    MCFM = " << res_mcfm[2] << std::endl;

      }
      std::cout << " Born:   MCFM = " << std::setprecision(17) << res_mcfm[3] << std::endl << std::endl;

      if (benchmark) {
        if (ampType == 11) {
          std::cout << " Finite: ratio = " << std::setprecision(17) << res_mcfm[0]/benchres[0] << std::endl
                    << " IR:     ratio = " << res_mcfm[1]/benchres[1] << std::endl
                    << " IR2:    ratio = " << res_mcfm[2]/benchres[2] << std::endl;
          if (abs(res_mcfm[0]/benchres[0] - 1) > benchtoler) passedbench = 0;
          if (abs(res_mcfm[1]/benchres[1] - 1) > benchtoler) passedbench = 0;
          if (abs(res_mcfm[2]/benchres[2] - 1) > benchtoler) passedbench = 0;
        }
        std::cout << " Born:   ratio = " << std::setprecision(17) << res_mcfm[3]/benchres[3] << std::endl << std::endl;
        if (abs(res_mcfm[3]/benchres[3] - 1) > benchtoler) passedbench = 0;
      }

      if (printMoms) {
        for (size_t i(0);i<pn.size();++i)
          std::cout << " p[" << i << "] = " << std::setprecision(17)
                    << pn[i] << " ( p^2 = " << m2(pn[i]) << " )\n";
        std::cout << std::endl;
      }
    }
  }
  else {
    t=clock();
    for (int iPt(0);iPt<nPts*100;++iPt) {
      // Generate phase space point with OpenLoops.
      std::vector<FourVec> pn;
      GenPoint(Ecm, pn, ids, nIn, params);
    }
    cput_ps=double(clock()-t)/100.;

    mcfm.SetPoleCheck(false);
    t=clock();
    for (int iPt(0);iPt<nPts*mcfm_nptsfac;++iPt) {
      // Generate phase space point with OpenLoops.
      std::vector<FourVec> pn;
      GenPoint(Ecm, pn, ids, nIn, params);
	mcfm.SetAlphaS(std::stod(params["alpha_S"]));
	mcfm.Calc(pid_mcfm, pn, 1);
    }
    cput_mcfm=(double(clock()-t)-cput_ps*mcfm_nptsfac)/CLOCKS_PER_SEC/mcfm_nptsfac;
  }

  if (benchmark) {
  // Print benchmark results.
    std::cout << "Benchmark with tolerance: " << std::scientific << benchtoler << std::endl;
    if (passedbench) {
      std::cout << "\033[1;32m PASSED \033[0m \n\n";
    }
    else {
      std::cout << "\033[1;31m FAILED \033[0m \n\n";
    }
  }
  else {
  // Print average CPU time.
  std::cout << " ----------------------------------------------------\n"
	    << "  Average CPU time per event:\n"
	    << "    MCFM:      " << std::setprecision(17) << std::scientific << cput_mcfm/double(nPts) << "s\n"
	    << " ----------------------------------------------------\n";
  }

  // Done.
  return 0;

}

