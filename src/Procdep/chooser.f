!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!

      subroutine chooser
          use types
!          use PDFerrors
!          use VVconfig_m, only: decayElAntiEl, decayQuarks, decayNuAntiNu,
!     &                          setDecayChannel
!          use singletop2_scale_m
!          use singletop2_decaywidth_m, only : singletop2_decaywidth
!          use singletop2_nnlo_vars, only: setup_singletop, currentContrib,
!     &          singletop2_nnlo_fully_inclusive
!          use anomcoup_tbW, only : anomcoup_init, enable_lambda2, anomcoup_cc, mode_anomcoup
!          use eftcouple, only: eftcouple_init
!          use scaleset_m, only: set_scales
!          use Scalevar
!          use PDFerrors
!          use mod_qcdloop_c
!          use Multichannel
          use, intrinsic :: ieee_arithmetic
      implicit none
c---- Note added 4/21/03
c---- plabel set to 'ig' (for 'ignore') means that this
c---- particle should not be subject to any cuts, so that the
c---- total cross-section comes out correctly when the BR is removed

      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'cplx.h'
      include 'masses.f'
      include 'ewcharge.f'
      include 'zcouple.f'
      include 'zerowidth.f'
      include 'removebr.f'
      include 'nwz.f'
      include 'kprocess.f'
      include 'flags.f'
      include 'nflav.f'
      include 'nodecay.f'
      include 'scale.f'
      include 'facscale.f'
      include 'nlooprun.f'
      include 'b0.f'
      include 'plabel.f'
      include 'couple.f'
      include 'kpart.f'
      include 'hdecaymode.f'
      include 'breit.f'
      include 'verbose.f'
      include 'nqcdjets.f'
      include 'nproc.f'
      include 'first.f'
      include 'ewcouple.f'
      include 'zcouple_cms.f'
      include 'blha.f'
      include 'maxd.f'
      include 'Cabibbo.f'
      include 'ewinput.f'
      include 'scalarselect.f'
      include 'toploops.f'
      real(dp):: wwbr,zzbr,tautaubr,gamgambr,zgambr,Rcut,Rbbmin,
     & alphas,cmass,bmass
      real(dp):: br,BrnRat,brwen,brzee,brznn,brtau,brtop,brcharm
      real(dp) :: branch_zqq
      integer:: mproc,j,isub,ilomomenta
      logical :: reset_alphaEM
      character(len=256):: pname
      character(len=1):: order
      character(len=82):: pwrite
      integer:: maxdip,dipconfig(maxd,3),maxperms,perm2(2),perm3(2)
      common/Rbbmin/Rbbmin
      common/Rcut/Rcut
      common/BrnRat/BrnRat
      common/isub/isub
      common/ilomomenta/ilomomenta
      common/qmass/cmass,bmass
      data hdecaymode/'xxxx'/

!      currentPDF = 0

      do j=1,mxpart
      plabel(j)=''
      enddo

c      do j=1,50
c      mcfmplotinfo(j)=-1
c      enddo

      open(unit=21,file='process.DAT',status='old',err=43,action='read',form='formatted')

!$omp master
      if (verbose ) then
          write(6,'(A,I3)') 'Chooser: process chosen by nproc = ',nproc
      endif
!$omp end master

      do j=1,600
      read(21,*,err=44) mproc,pname,order

c      if (rank == 0) then
         if (nproc < 0) then
            write(6,*) mproc,pname
         endif
c      endif
      if (mproc == nproc) go to 42
      if (pname == 'EOF') go to 44
      enddo
      goto 44

 42   continue
      if (verbose .and. first) then
       write(6,*)
       write(6,*) '*************************** f(p1)+f(p2) --> *****'//
     &           '*************************************'
      j=index(pname,'[')
      if (j > 101) j=101
      if (j > 0) then
        pwrite=adjustl(pname(19:j-1))
        write(6,99) pwrite
        pwrite=adjustl(pname(j:len(pname)))
        write(6,99) pwrite
      else
        pwrite=adjustl(pname(19:100))
        write(6,99) pwrite
      endif
      write(6,*) '*************************************************'//
     & '*************************************'
      write(6,*)
      endif
      close(unit=21)

c--- check no. of momenta appearing in LO process, fill ilomomenta common block
      if (first) then
        if (index(pname,'p3') > 0)  ilomomenta=3
        if (index(pname,'p4') > 0)  ilomomenta=4
        if (index(pname,'p5') > 0)  ilomomenta=5
        if (index(pname,'p6') > 0)  ilomomenta=6
        if (index(pname,'p7') > 0)  ilomomenta=7
        if (index(pname,'p8') > 0)  ilomomenta=8
        if (index(pname,'p9') > 0)  ilomomenta=9
        if (index(pname,'p10') > 0) ilomomenta=10
        if (index(pname,'p11') > 0) ilomomenta=11
        if (index(pname,'p12') > 0) ilomomenta=12
      endif

      plabel(1)='pp'
      plabel(2)='pp'

c--- the default behaviour is to remove no branching ratio
      BrnRat=1._dp

c--- catch processes for which complex mass scheme should be enforced
      if (kcase == kWln_ew) then
        ewscheme=4
      endif
c--- set up most parameters
      call coupling

      nqcdjets=0
      isub=0
      nodecay=.false.
      reset_alphaEM=.false.
c      lastphot=-1
c--- default is no interference contributions from identical fermions
c--- default is weak process

c-- Rbbmin is an additional variable, added so that the separation
c-- between two b jets can be controlled separately from the Delta_R
c-- cut between any other types of jet
c-- Default behaviour: the same value as for the other jets
      Rbbmin=Rcut

c-----------------------------------------------------------------------

      if ((nproc == 1) .or. (nproc == 6)) then
        kcase=kW_only
        mass3=wmass
        width3=wwidth
        n3=1
c        ndim=4
        nqcdjets=0

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

c---W^+
        if     (nproc == 1) then
c-- 1  '  f(p1)+f(p2) --> W^+(-->nu(p3)+e^+(p4))'
c--    '  f(p1)+f(p2) --> W^+ (for total Xsect)' (removebr=.true.)
          plabel(3)='nl'
          plabel(4)='ea'
          plabel(5)='pp'
          nwz=1
c---W^-
        elseif (nproc == 6) then
c-- 6  '  f(p1)+f(p2) --> W^-(-->e^-(p3)+nu~(p4))'
c--    '  f(p1)+f(p2) --> W^- (for total Xsect)' (removebr=.true.)
          plabel(3)='el'
          plabel(4)='na'
          plabel(5)='pp'
          nwz=-1
        else
          call nprocinvalid()
        endif

c--- total cross-section
        if (removebr) then
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brwen
          plabel(3)='ig'
          plabel(4)='ig'
        endif


      elseif ((nproc == 11) .or. (nproc == 16)) then
        kcase=kW_1jet
        nqcdjets=1
c        ndim=7
        mb=0
        n2=0
        n3=1
        mass3=wmass
        width3=wwidth
        plabel(5)='pp'
        plabel(6)='pp'

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

        if     (nproc == 11) then
c-- 11 '  f(p1)+f(p2) --> W^+(-->nu(p3)+e^+(p4))+f(p5)'
c--    '  f(p1)+f(p2) --> W^+ (no BR) + f(p5)' (removebr=.true.)
          nwz=1
          plabel(3)='nl'
          plabel(4)='ea'
        elseif (nproc == 16) then
c-- 16 '  f(p1)+f(p2) --> W^-(-->e^-(p3)+nu~(p4))+f(p5)'
c--    '  f(p1)+f(p2) --> W^- (no BR) + f(p5)' (removebr=.true.)
          nwz=-1
          plabel(3)='el'
          plabel(4)='na'
        endif

c--- total cross-section
        if (removebr) then
          plabel(3)='ig'
          plabel(4)='ig'
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brwen
        endif

      elseif ((nproc == 22) .or. (nproc == 27)) then
        kcase=kW_2jet
        nqcdjets=2
        plabel(5)='pp'
        plabel(6)='pp'
        plabel(7)='pp'
c        ndim=10
        n2=0
        n3=1
        mass3=wmass
        width3=wwidth

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

        toploops=texact ! exact treatment of top-quark loops
        toplight=.true.

        if     (nproc == 22) then
c-- 22 '  f(p1)+f(p2) --> W^+(-->nu(p3)+e^+(p4))+f(p5)+f(p6)'
c--    '  f(p1)+f(p2) --> W^+ (no BR) +f(p5)+f(p6)' (removebr=.true.)
          nwz=1
          plabel(3)='nl'
          plabel(4)='ea'
        elseif (nproc == 27) then
c-- 27 '  f(p1)+f(p2) --> W^-(-->e^-(p3)+nu~(p4)) + f(p5)+f(p6)'
c--    '  f(p1)+f(p2) --> W^- (no BR) +f(p5)+f(p6)' (removebr=.true.)
          nwz=-1
          plabel(3)='el'
          plabel(4)='na'
        endif

c--- total cross-section
        if (removebr) then
          plabel(3)='ig'
          plabel(4)='ig'
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brwen
        endif

      elseif ((nproc > 30) .and. (nproc <= 35)) then
        kcase=kZ_only
        nqcdjets=0
        nwz=0
        mass3=zmass
        width3=zwidth
        n3=1
c        ndim=4
        plabel(5)='pp'

c        new_pspace = .false.

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

        if     (nproc == 31) then
c-- 31 '  f(p1)+f(p2) --> Z^0(-->e^-(p3)+e^+(p4))'
c--    '  f(p1)+f(p2) --> Z^0 (for total Xsect)' (removebr=.true.)
          call checkminzmass(1)
          plabel(3)='el'
          plabel(4)='ea'
          q1=-1._dp
          ! to set photon contribution to zero
          !q1 = 0._dp
          l1=le
          r1=re
          zl1=zle
          zr1=zre
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
!            call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
            BrnRat=brzee
          endif
        elseif (nproc == 32) then
c-- 32 '  f(p1)+f(p2) --> Z^0(-->3*(nu(p3)+nu~(p4)))'
          plabel(3)='nl'
          plabel(4)='na'
          q1=zip
          l1=ln*sqrt(3._dp)
          r1=rn*sqrt(3._dp)
          zl1=zln*sqrt(3._dp)
          zr1=zrn*sqrt(3._dp)
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
!            call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
            BrnRat=brznn
          endif
        elseif (nproc == 33) then
c-- 33 '  f(p1)+f(p2) --> Z^0(-->b(p3)+b~(p4))'
          call checkminzmass(1)
          plabel(3)='qb'
          plabel(4)='ab'
          q1=Q(5)*sqrt(xn)
          l1=l(5)*sqrt(xn)
          r1=r(5)*sqrt(xn)
        elseif (nproc == 34) then
c-- 34 '  f(p1)+f(p2) --> Z^0(-->3*(d(p3)+d~(p4)))'
          call checkminzmass(1)
          plabel(3)='qb'
          plabel(4)='ab'
          q1=Q(1)*sqrt(3._dp*xn)
          l1=l(1)*sqrt(3._dp*xn)
          r1=r(1)*sqrt(3._dp*xn)
        elseif (nproc == 35) then
c-- 35 '  f(p1)+f(p2) --> Z^0(-->2*(u(p3)+u~(p4)))'
          call checkminzmass(1)
          plabel(3)='qb'
          plabel(4)='ab'
          q1=Q(2)*sqrt(2._dp*xn)
          l1=l(2)*sqrt(2._dp*xn)
          r1=r(2)*sqrt(2._dp*xn)
        else
          call nprocinvalid()
        endif


      elseif ((nproc >= 41) .and. (nproc <= 43)) then
        kcase=kZ_1jet
        nqcdjets=1
        nwz=0
c        ndim=7
        mb=0
        n2=0
        n3=1
        mass3=zmass
        width3=zwidth
        plabel(5)='pp'
        plabel(6)='pp'

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

        if     (nproc == 41) then
c-- 41 '  f(p1)+f(p2) --> Z^0(-->e^-(p3)+e^+(p4))+f(p5)'
c--    '  f(p1)+f(p2) --> Z^0 (no BR) +f(p5)' (removebr=.true.)
          call checkminzmass(1)
          plabel(3)='el'
          plabel(4)='ea'
          q1=-1._dp
          ! to set photon contribution to zero
          !q1 = 0._dp
          l1=le
          r1=re
          zl1=zle
          zr1=zre
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
!            call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
            BrnRat=brzee
          endif
        elseif (nproc == 42) then
c-- 42 '  f(p1)+f(p2) --> Z_0(-->3*(nu(p3)+nu~(p4)))-(sum over 3 nu)+f(p5)'
            plabel(3)='nl'
            plabel(4)='na'
            q1=zip
            l1=ln*sqrt(3._dp)
            r1=rn*sqrt(3._dp)
            zl1=zln*sqrt(3._dp)
            zr1=zrn*sqrt(3._dp)
            if (removebr) then
              plabel(3)='ig'
              plabel(4)='ig'
!              call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
              BrnRat=brznn
            endif
        elseif (nproc == 43) then
c-- 43 '  f(p1)+f(p2) --> Z^0(-->b(p3)+b~(p4))+f(p5)'
          call checkminzmass(1)
          plabel(3)='qb'
          plabel(4)='ab'
          q1=Q(5)*sqrt(xn)
          l1=l(5)*sqrt(xn)
          r1=r(5)*sqrt(xn)
        endif

      elseif (nproc == 44) then
c-- 44 '  f(p1)+f(p2) --> Z^0(-->e^-(p3)+e^+(p4))+f(p5)+f(p6)'
c--    '  f(p1)+f(p2) --> Z^0 (no BR) +f(p5)+f(p6)' (removebr=.true.)
        kcase=kZ_2jet
        call checkminzmass(1)
c        ndim=10
        n2=0
        n3=1
        nqcdjets=2
        plabel(3)='el'
        plabel(4)='ea'
        plabel(5)='pp'
        plabel(6)='pp'
        plabel(7)='pp'
        q1=-1._dp
        l1=le
        r1=re
        zl1=zle
        zr1=zre
        nwz=0
        mass3=zmass
        width3=zwidth

        toploops=texact ! exact treatment of top-quark loops
        toplight=.true.
        topaxial=.true.
        topvector=.true.

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

c--- total cross-section
        if (removebr) then
          plabel(3)='ig'
          plabel(4)='ig'
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brzee
        endif

      elseif (nproc == 45) then
c-- 45 '  f(p1)+f(p2) --> Z^0(-->e^-(p3)+e^+(p4))+f(p5)+f(p6)+f(p7)'
c--    '  f(p1)+f(p2) --> Z^0 (no BR) +f(p5)+f(p6)+f(p7)' (removebr=.true.)
        kcase=kZ_3jet
        call checkminzmass(1)
c        ndim=13
        n2=0
        n3=1
        nqcdjets=3
        plabel(3)='el'
        plabel(4)='ea'
        plabel(5)='pp'
        plabel(6)='pp'
        plabel(7)='pp'
        q1=-1._dp
        l1=le
        r1=re
        zl1=zle
        zr1=zre
        nwz=0
        mass3=zmass
        width3=zwidth

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

c--- total cross-section
        if (removebr) then
          plabel(3)='ig'
          plabel(4)='ig'
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brzee
        endif

      elseif (nproc == 46) then
c-- 46 '  f(p1)+f(p2) --> Z^0(-->3*(nu(p3)+nu~(p4))+f(p5)+f(p6)'
        kcase=kZ_2jet
c        ndim=10
        n2=0
        n3=1
        nqcdjets=2
        plabel(3)='nl'
        plabel(4)='na'
        plabel(5)='pp'
        plabel(6)='pp'
        plabel(7)='pp'
        q1=zip
        l1=ln*sqrt(3._dp)
        r1=rn*sqrt(3._dp)
        zl1=zln*sqrt(3._dp)
        zr1=zrn*sqrt(3._dp)
        nwz=0
        mass3=zmass
        width3=zwidth

        toploops=texact ! exact treatment of top-quark loops
        toplight=.true.
        topaxial=.true.
        topvector=.true.

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

c--- total cross-section
        if (removebr) then
          plabel(3)='ig'
          plabel(4)='ig'
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brznn
        endif

      elseif (nproc == 47) then
c-- 47 '  f(p1)+f(p2) --> Z^0(-->3*(nu(p3)+nu~(p4))+f(p5)+f(p6)+f(p7)'
        kcase=kZ_3jet
c        ndim=13
        n2=0
        n3=1
        nqcdjets=3
        plabel(3)='nl'
        plabel(4)='na'
        plabel(5)='pp'
        plabel(6)='pp'
        plabel(7)='pp'
        q1=zip
        l1=ln*sqrt(3._dp)
        r1=rn*sqrt(3._dp)
        zl1=zln*sqrt(3._dp)
        zr1=zrn*sqrt(3._dp)
        nwz=0
        mass3=zmass
        width3=zwidth

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

c--- total cross-section
        if (removebr) then
          plabel(3)='ig'
          plabel(4)='ig'
!          call branch(brwen,brzee,brznn,brtau,brtop,brcharm)
          BrnRat=brznn
        endif

       elseif ((nproc == 111) .or. (nproc == 112)
     &    .or. (nproc == 119)) then
        kcase=kggfus0
!        call sethparams(br,wwbr,zzbr,tautaubr,gamgambr,zgambr)
        plabel(5)='pp'
c        ndim=4

        n2=0
        n3=1
        mass3=hmass
        width3=hwidth

c        new_pspace = .false.

c        mcfmplotinfo= (/ 34, (0,j=1,49) /)

        if     (nproc == 111) then
c--  111 '  f(p1)+f(p2) --> H(-->b(p3)+bbar(p4))'
c--      '  f(p1)+f(p2) --> H (for total Xsect)' (removebr=.true.)
          hdecaymode='bqba'
          plabel(3)='bq'
          plabel(4)='ba'
          nqcdjets=2
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
            nqcdjets=0
            BrnRat=br
          endif

        elseif (nproc == 112) then
c--  112 '  f(p1)+f(p2) --> H(-->tau^-(p3)+tau^+(p4))'
c--      '  f(p1)+f(p2) --> H (for total Xsect)' (removebr=.true.)
          hdecaymode='tlta'
          plabel(3)='tl'
          plabel(4)='ta'
          nqcdjets=0
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
            BrnRat=tautaubr
          endif

        elseif (nproc == 119) then
c--  119 '  f(p1)+f(p2) --> H(-->gamma^-(p3)+gamma^+(p4))'
c--      '  f(p1)+f(p2) --> H (for total Xsect)' (removebr=.true.)
          hdecaymode='gaga'
          plabel(3)='ga'
          plabel(4)='ga'
          hdecaymode='gaga'
          nqcdjets=0
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
            BrnRat=gamgambr
          endif
        endif

      elseif ((nproc == 269) .or. (nproc == 270) .or. (nproc == 2691)
     &   .or. (nproc == 271)
     &   .or. (nproc == 272)) then

c--- turn off Higgs decay, for speed
c        nodecay=.true.
c--- parameters to turn off various pieces, for checking
!        f0q=one
!        f2q=one
!        f4q=one

!        call sethparams(br,wwbr,zzbr,tautaubr,gamgambr,zgambr)

        plabel(5)='pp'
        plabel(6)='pp'
        plabel(7)='pp'
!        ndim=10

        n2=0
        n3=1

        mass3=hmass
        width3=hwidth

!        mcfmplotinfo= (/ 34, 56, (0,j=1,48) /)

        if     ((nproc == 269) .or. (nproc == 270)) then
c-- 270 '  f(p1)+f(p2) --> H(gamma(p3)+gamma(p4))+f(p5)+f(p6)[in heavy top limit]'
c--     '  f(p1)+f(p2) --> H(no BR)+f(p5)+f(p6)[in heavy top limit]' (removebr=.true.)
          hdecaymode='gaga'
          plabel(3)='ga'
          plabel(4)='ga'
          kcase=kgagajj
          if (nproc == 269) kcase=kh2jmas
          nqcdjets=2
          if (removebr) then
            plabel(3)='ig'
            plabel(4)='ig'
            BrnRat=gamgambr
          endif
        endif

      else
        call nprocinvalid()
      endif

c--- set notag (may be modified by user, with care!)
!      call setnotag()

c--- set up alpha-s again (in case nflav was changed)
!      call coupling2

c--- remove 2 dimensions from integration if decay is not included
c      if (nodecay) ndim=ndim-2

c--- report on the removed BR, if necessary
      if (removebr) then
c       if (rank == 0) then
        write(6,*)'****************************************************'
c       endif
       if (BrnRat  /=  1._dp) then
c        if (rank == 0) then
        write(6,*)'*             Setting zerowidth to .true.          *'
c        endif
        zerowidth=.true.
       endif
c       if (rank == 0) then
        write(6,98) BrnRat
        write(6,*)'****************************************************'
c       endif
      endif

c--- if needed for photon proceses reset to 1/137.0
c      if(reset_alphaEM) call reset_aem(aem)
      if (useblha == 0) then
         if(reset_alphaEM) call reset_aem(1._dp/137.0_dp)
      endif

c--- initialize arrays that are used in is_functions
c      call init_is_functions()

c--- fill up CKM matrix
      call ckmfill(nwz)

c--- set flags to true unless we're doing W+2 jet or Z+2 jet
      if ( ((kcase /= kW_2jet) .and. (kcase /= kZ_2jet))
     & .or. (kpart==klord) ) then
        Qflag=.true.
        Gflag=.true.
      endif

c fix complex-mass scheme couplings in case not already set above!
      call fixcms(l1,zl1)
      call fixcms(r1,zr1)
      call fixcms(l2,zl2)
      call fixcms(r2,zr2)

c-- check that new_pspace will work for this process and fix otherwise
!      if (new_pspace .and. (nproc /= 1610 .and. nproc /= 1650)) then
!        call dipoleconfig(maxdip,dipconfig,maxperms,perm2,perm3)
!        if (maxperms < 1) then
!          write(6,*) 'Setting new_pspace = .false. !'
!          new_pspace = .false.
!        endif
!      endif

c-- set QCDLoop cache size
!      if (first) call qlcachesize(200)

      if (first) first = .false.

      return

 43   write(6,*) 'problems opening process.DAT'
      stop

 44   write(6,*) 'Unimplemented process number, nproc = ',nproc,
     & ' mcfm halted'
      stop

 98   format(' *             Brn.Rat. removed = ',  f11.7, '       *')
 99   format(' * ',a82,' *')

      end

      subroutine nprocinvalid()
      implicit none
      include 'types.f'
      include 'nproc.f'

      write(6,*) 'chooser: Unimplemented case'
      write(6,*) 'nproc=',nproc
      stop

      return
      end

      subroutine checkminzmass(i)
      implicit none
      include 'types.f'
      include 'constants.f'
c--- Checks that the minimum invariant mass specified in the options
c--- file is not zero for boson 34 (i=1) or boson 56 (i=2)

      include 'limits.f'
      include 'zerowidth.f'
      integer:: i

c--- if generating exactly on-shell, there's nothing to worry about
      if (zerowidth) return

      if ((i == 1) .and. (wsqmin == zip)) then
        write(6,*)
        write(6,*) 'Please set m34min not equal to zero to'
        write(6,*) 'prevent the virtual photon from becoming real.'
        stop
      endif

      if ((i == 2) .and. (bbsqmin == zip)) then
        write(6,*)
        write(6,*) 'Please set m56min not equal to zero to'
        write(6,*) 'prevent the virtual photon from becoming real.'
        stop
      endif

      return
      end


