!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
      subroutine A51mmppp5g(j1,j2,j3,j4,j5,za,zb,miA5tree,A51half,A51one)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      include 'sprods_com.f'
      include 'zprods_decl.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'scale.f'
      real(dp), parameter:: deltar=1._dp ! t'Hooft-Veltman scheme (FDH -> deltar = 0)
      complex(dp):: A51half,A51one
      complex(dp):: Vg,Vf,Vs,Ff,Fs,L0,L2,miA5tree,lnrat
      integer:: j1,j2,j3,j4,j5
c----Eq.(8,10) of hep-ph/9302280v1 of BDK multiplied by 16*pi^2*(-i)
c--- to give (16*pi^2)*(-i)*A^{[1/2]}_{5;1}
      miA5tree=za(j1,j2)**4
     & /(za(j1,j2)*za(j2,j3)*za(j3,j4)*za(j4,j5)*za(j5,j1))
      Vg=-five*epinv*epinv2
     & -epinv*(lnrat(musq,-s(j1,j2))+lnrat(musq,-s(j2,j3))+lnrat(musq,-s(j3,j4))
     &        +lnrat(musq,-s(j4,j5))+lnrat(musq,-s(j5,j1)))
     & -half*(lnrat(musq,-s(j1,j2))**2+lnrat(musq,-s(j2,j3))**2+lnrat(musq,-s(j3,j4))**2
     &        +lnrat(musq,-s(j4,j5))**2+lnrat(musq,-s(j5,j1))**2)
     & +lnrat(-s(j1,j2),-s(j2,j3))*lnrat(-s(j3,j4),-s(j4,j5))
     & +lnrat(-s(j2,j3),-s(j3,j4))*lnrat(-s(j4,j5),-s(j5,j1))
     & +lnrat(-s(j3,j4),-s(j4,j5))*lnrat(-s(j5,j1),-s(j1,j2))
     & +lnrat(-s(j4,j5),-s(j5,j1))*lnrat(-s(j1,j2),-s(j2,j3))
     & +lnrat(-s(j5,j1),-s(j1,j2))*lnrat(-s(j2,j3),-s(j3,j4))
     & +five/six*pisq-deltar/three
      Vf=cmplx(-five/two*epinv-two,kind=dp)
     & -half*(lnrat(musq,-s(j2,j3))+lnrat(musq,-s(j5,j1)))
      Vs=-Vf/three+cmplx(two/nine,kind=dp)
      Ff=-za(j1,j2)**2*L0(-s(j2,j3),-s(j5,j1))
     & *(za(j2,j3)*zb(j3,j4)*za(j4,j1)+za(j2,j4)*zb(j4,j5)*za(j5,j1))
     & /(two*za(j2,j3)*za(j3,j4)*za(j4,j5)*za(j5,j1)*s(j5,j1))
      Fs=-Ff/three
     & -zb(j3,j4)*za(j4,j1)*za(j2,j4)*zb(j4,j5)*L2(-s(j2,j3),-s(j5,j1))
     & *(za(j2,j3)*zb(j3,j4)*za(j4,j1)+za(j2,j4)*zb(j4,j5)*za(j5,j1))
     & /(three*za(j3,j4)*za(j4,j5)*s(j5,j1)**3)
     & -za(j3,j5)*zb(j3,j5)**3
     & /(three*zb(j1,j2)*zb(j2,j3)*za(j3,j4)*za(j4,j5)*zb(j5,j1))
     & +za(j1,j2)*zb(j3,j5)**2
     & /(three*zb(j2,j3)*za(j3,j4)*za(j4,j5)*zb(j5,j1))
     & +za(j1,j2)*zb(j3,j4)*za(j4,j1)*za(j2,j4)*zb(j4,j5)
     & /(six*s(j2,j3)*za(j3,j4)*za(j4,j5)*s(j5,j1))
      A51half=-(Vf+Vs)*miA5tree-(Ff+Fs)
      A51one=(Vg+four*Vf+Vs)*miA5tree+(four*Ff+Fs)

c      write(6,*) 'j1,j2,j3,j4,j5,miA5tree',j1,j2,j3,j4,j5,miA5tree
      return
      end
