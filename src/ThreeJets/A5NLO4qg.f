      function A5NLO4qg(j1,j2,j3,j4,j5,identical)
! Implementation of one-loop x tree interference for:
!
! 0 -> q~(j1) + Q~(j2) + Q(j3) + q(j4) + g(j5)
!
! Result taken from:
!
! 'One loop radiative corrections to the helicity amplitudes
!  of QCD processes involving four quarks and one gluon'
!  Zoltan Kunszt, Adrian Signer, Zoltan Trocsanyi
!  Published in: Phys.Lett.B 336 (1994) 529-536
!  hep-ph/9405386
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'qcdcouple.f'
      integer j1,j2,j3,j4,j5,hq,hQQ,hg
      logical identical
      complex(dp) :: aQQ_qb0_hel(2,2,2),aQQ_QQb0_hel(2,2,2),aq_qb0_hel(2,2,2),aq_QQb0_hel(2,2,2)
      complex(dp) :: aQQ_qb1_hel(2,2,2),aQQ_QQb1_hel(2,2,2),aq_qb1_hel(2,2,2),aq_QQb1_hel(2,2,2)
      complex(dp) :: aQQ_qb0_hel_exch(2,2,2),aQQ_QQb0_hel_exch(2,2,2),
     & aq_qb0_hel_exch(2,2,2),aq_QQb0_hel_exch(2,2,2)
      complex(dp) :: aQQ_qb1_hel_exch(2,2,2),aQQ_QQb1_hel_exch(2,2,2),
     & aq_qb1_hel_exch(2,2,2),aq_QQb1_hel_exch(2,2,2)
      real(dp) :: A5NLO4qg
      
      call A5NLO4qg_hel(j1,j2,j3,j4,j5,
     & aQQ_qb0_hel,aQQ_QQb0_hel,aq_qb0_hel,aq_QQb0_hel,
     & aQQ_qb1_hel,aQQ_QQb1_hel,aq_qb1_hel,aq_QQb1_hel)
      
! perform complex conjugation on tree amplitudes, in preparation for sum
      aQQ_qb0_hel=conjg(aQQ_qb0_hel)
      aQQ_QQb0_hel=conjg(aQQ_QQb0_hel)
      aq_qb0_hel=conjg(aq_qb0_hel)
      aq_QQb0_hel=conjg(aq_QQb0_hel)

      if (identical) then
        call A5NLO4qg_hel(j2,j1,j3,j4,j5,
     &   aQQ_qb0_hel_exch,aQQ_QQb0_hel_exch,aq_qb0_hel_exch,aq_QQb0_hel_exch,
     &   aQQ_qb1_hel_exch,aQQ_QQb1_hel_exch,aq_qb1_hel_exch,aq_QQb1_hel_exch)
! perform complex conjugation on tree amplitudes, in preparation for sum
        aQQ_qb0_hel_exch=conjg(aQQ_qb0_hel_exch)
        aQQ_QQb0_hel_exch=conjg(aQQ_QQb0_hel_exch)
        aq_qb0_hel_exch=conjg(aq_qb0_hel_exch)
        aq_QQb0_hel_exch=conjg(aq_QQb0_hel_exch)
      endif
      
      A5NLO4qg=zip
! note: color sum is the same at tree-level and one-loop
      do hq=1,2
      do hQQ=1,2
      do hg=1,2
      if ((identical) .and. (hq == hQQ)) then
        A5NLO4qg=A5NLO4qg+V*real(
     &   xn*(aq_QQb0_hel(hq,hQQ,hg)*aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb0_hel(hq,hQQ,hg)*aQQ_qb1_hel(hq,hQQ,hg)
     &      +aq_QQb0_hel_exch(hq,hQQ,hg)*aq_QQb1_hel_exch(hq,hQQ,hg)+aQQ_qb0_hel_exch(hq,hQQ,hg)*aQQ_qb1_hel_exch(hq,hQQ,hg))
     &  +(
     &   + aq_qb0_hel(hq,hQQ,hg)*aQQ_qb1_hel_exch(hq,hQQ,hg)
     &   + aq_qb0_hel_exch(hq,hQQ,hg)*aQQ_qb1_hel(hq,hQQ,hg)
     &   + aQQ_QQb0_hel(hq,hQQ,hg)*aq_QQb1_hel_exch(hq,hQQ,hg)
     &   + aQQ_QQb0_hel_exch(hq,hQQ,hg)*aq_QQb1_hel(hq,hQQ,hg)
     &   - aq_QQb0_hel(hq,hQQ,hg)*(aq_QQb1_hel_exch(hq,hQQ,hg)+aQQ_qb1_hel_exch(hq,hQQ,hg)-aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &   - aq_QQb0_hel_exch(hq,hQQ,hg)*(aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb1_hel(hq,hQQ,hg)-aQQ_QQb1_hel(hq,hQQ,hg))
     &   - aQQ_qb0_hel(hq,hQQ,hg)*(aq_QQb1_hel_exch(hq,hQQ,hg)+aQQ_qb1_hel_exch(hq,hQQ,hg)-aq_qb1_hel_exch(hq,hQQ,hg))
     &   - aQQ_qb0_hel_exch(hq,hQQ,hg)*(aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb1_hel(hq,hQQ,hg)-aq_qb1_hel(hq,hQQ,hg))
     &   )
     &  +one/xn*(
     &   - aq_QQb0_hel(hq,hQQ,hg)*(aq_qb1_hel(hq,hQQ,hg)+aQQ_QQb1_hel(hq,hQQ,hg))
     &   - aq_QQb0_hel_exch(hq,hQQ,hg)*(aq_qb1_hel_exch(hq,hQQ,hg)+aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &   - aQQ_qb0_hel(hq,hQQ,hg)*(aq_qb1_hel(hq,hQQ,hg)+aQQ_QQb1_hel(hq,hQQ,hg))
     &   - aQQ_qb0_hel_exch(hq,hQQ,hg)*(aq_qb1_hel_exch(hq,hQQ,hg)+aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &   - aq_qb0_hel(hq,hQQ,hg)*(aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb1_hel(hq,hQQ,hg)-aq_qb1_hel(hq,hQQ,hg))
     &   - aq_qb0_hel_exch(hq,hQQ,hg)*(aq_QQb1_hel_exch(hq,hQQ,hg)+aQQ_qb1_hel_exch(hq,hQQ,hg)-aq_qb1_hel_exch(hq,hQQ,hg))
     &   - aQQ_QQb0_hel(hq,hQQ,hg)*(aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb1_hel(hq,hQQ,hg)-aQQ_QQb1_hel(hq,hQQ,hg))
     &   - aQQ_QQb0_hel_exch(hq,hQQ,hg)*(aq_QQb1_hel_exch(hq,hQQ,hg)+aQQ_qb1_hel_exch(hq,hQQ,hg)-aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &  )
     &  -one/xn**2*(
     &   + aq_qb0_hel(hq,hQQ,hg)*(aq_qb1_hel_exch(hq,hQQ,hg)+aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &   + aq_qb0_hel_exch(hq,hQQ,hg)*(aq_qb1_hel(hq,hQQ,hg)+aQQ_QQb1_hel(hq,hQQ,hg))
     &   + aQQ_QQb0_hel(hq,hQQ,hg)*(aq_qb1_hel_exch(hq,hQQ,hg)+aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &   + aQQ_QQb0_hel_exch(hq,hQQ,hg)*(aq_qb1_hel(hq,hQQ,hg)+aQQ_QQb1_hel(hq,hQQ,hg)))
     &  ,kind=dp)
      elseif (identical) then
        A5NLO4qg=A5NLO4qg+V*real(
     &   xn*(aq_QQb0_hel(hq,hQQ,hg)*aq_QQb1_hel(hq,hQQ,hg)
     &      +aQQ_qb0_hel(hq,hQQ,hg)*aQQ_qb1_hel(hq,hQQ,hg))
     &   -one/xn*(
     &    + (aq_QQb0_hel(hq,hQQ,hg)+aQQ_qb0_hel(hq,hQQ,hg))
     &      *(aq_qb1_hel(hq,hQQ,hg)+aQQ_QQb1_hel(hq,hQQ,hg))
     &    + (aq_qb0_hel(hq,hQQ,hg)+aQQ_QQb0_hel(hq,hQQ,hg))
     &      *(aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb1_hel(hq,hQQ,hg))
     &    - aq_qb0_hel(hq,hQQ,hg)*aq_qb1_hel(hq,hQQ,hg)
     &    - aQQ_QQb0_hel(hq,hQQ,hg)*aQQ_QQb1_hel(hq,hQQ,hg) ),kind=dp)
     &          +V*real(
     &   xn*(aq_QQb0_hel_exch(hq,hQQ,hg)*aq_QQb1_hel_exch(hq,hQQ,hg)
     &      +aQQ_qb0_hel_exch(hq,hQQ,hg)*aQQ_qb1_hel_exch(hq,hQQ,hg))
     &   -one/xn*(
     &    + (aq_QQb0_hel_exch(hq,hQQ,hg)+aQQ_qb0_hel_exch(hq,hQQ,hg))
     &      *(aq_qb1_hel_exch(hq,hQQ,hg)+aQQ_QQb1_hel_exch(hq,hQQ,hg))
     &    + (aq_qb0_hel_exch(hq,hQQ,hg)+aQQ_QQb0_hel_exch(hq,hQQ,hg))
     &      *(aq_QQb1_hel_exch(hq,hQQ,hg)+aQQ_qb1_hel_exch(hq,hQQ,hg))
     &    - aq_qb0_hel_exch(hq,hQQ,hg)*aq_qb1_hel_exch(hq,hQQ,hg)
     &    - aQQ_QQb0_hel_exch(hq,hQQ,hg)*aQQ_QQb1_hel_exch(hq,hQQ,hg) ),kind=dp)
      else
        A5NLO4qg=A5NLO4qg+V*real(
     &   xn*(aq_QQb0_hel(hq,hQQ,hg)*aq_QQb1_hel(hq,hQQ,hg)
     &      +aQQ_qb0_hel(hq,hQQ,hg)*aQQ_qb1_hel(hq,hQQ,hg))
     &   -one/xn*(
     &    + (aq_QQb0_hel(hq,hQQ,hg)+aQQ_qb0_hel(hq,hQQ,hg))
     &      *(aq_qb1_hel(hq,hQQ,hg)+aQQ_QQb1_hel(hq,hQQ,hg))
     &    + (aq_qb0_hel(hq,hQQ,hg)+aQQ_QQb0_hel(hq,hQQ,hg))
     &      *(aq_QQb1_hel(hq,hQQ,hg)+aQQ_qb1_hel(hq,hQQ,hg))
     &    - aq_qb0_hel(hq,hQQ,hg)*aq_qb1_hel(hq,hQQ,hg)
     &    - aQQ_QQb0_hel(hq,hQQ,hg)*aQQ_QQb1_hel(hq,hQQ,hg) ),kind=dp)
      endif
      enddo
      enddo
      enddo

! overall coupling factors
      A5NLO4qg=A5NLO4qg*2*gsq**3*ason4pi
      
! identical particle factor
      if (identical) A5NLO4qg=half*A5NLO4qg

      return
      end


      subroutine A5NLO4qg_hel(j1,j2,j3,j4,j5,
     & aQQ_qb0_hel,aQQ_QQb0_hel,aq_qb0_hel,aq_QQb0_hel,
     & aQQ_qb1_hel,aQQ_QQb1_hel,aq_qb1_hel,aq_QQb1_hel)
! Implementation of one-loop x tree interference for:
!
! 0 -> q~(j1) + Q~(j2) + Q(j3) + q(j4) + g(j5)
!
! Result taken from:
!
! 'One loop radiative corrections to the helicity amplitudes
!  of QCD processes involving four quarks and one gluon'
!  Zoltan Kunszt, Adrian Signer, Zoltan Trocsanyi
!  Published in: Phys.Lett.B 336 (1994) 529-536
!  hep-ph/9405386
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      include 'sprods_com.f'
      include 'zprods_com.f'
      integer j1,j2,j3,j4,j5,hq,hQQ,j,k,hg
      complex(dp) :: aQQ_qb0(2,2),aQQ_QQb0(2,2),aq_qb0(2,2),aq_QQb0(2,2)
      complex(dp) :: aQQ_qb1(2,2),aQQ_QQb1(2,2),aq_qb1(2,2),aq_QQb1(2,2)
      complex(dp) :: aQQ_qb0_hel(2,2,2),aQQ_QQb0_hel(2,2,2),aq_qb0_hel(2,2,2),aq_QQb0_hel(2,2,2)
      complex(dp) :: aQQ_qb1_hel(2,2,2),aQQ_QQb1_hel(2,2,2),aq_qb1_hel(2,2,2),aq_QQb1_hel(2,2,2)
      real(dp) :: p(mxpart,4),ss(mxpart,mxpart),sswap(mxpart,mxpart),scheme
      complex(dp) :: xa(mxpart,mxpart),xb(mxpart,mxpart)
      complex(dp) :: xaswap(mxpart,mxpart),xbswap(mxpart,mxpart)
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
      integer, parameter :: q=1, QQ=2, qb=3, QQb=4, g=5
! interchange of q and Q
      integer, parameter, dimension(5) :: qQswap = [ 2, 1, 4, 3, 5 ]
! interchange qb <-> q, Qbar <-> Q, c.f. Eqs. (14) and (15)
      integer, parameter, dimension(5) :: bar = [ 3, 4, 1, 2, 5 ]
! interchange qb <-> Q, q <-> Qbar, c.f. Eq. (16)
      integer, parameter, dimension(5) :: exch = [ 4, 3, 2, 1, 5 ]
      complex(dp) :: aQQ_qb0swap(2,2),aQQ_QQb0swap(2,2)
      complex(dp) :: aQQ_qb1swap(2,2),aQQ_QQb1swap(2,2)
      complex(dp) :: SQQ_qb(2,2),SQQ_QQb(2,2),RQQ_qb(2,2),RQQ_QQb(2,2),CQQ_QQb(2,2),CQQ_qb(2,2)
      complex(dp) :: SQQ_qbswap(2,2),SQQ_QQbswap(2,2),RQQ_qbswap(2,2),RQQ_QQbswap(2,2),
     & CQQ_QQbswap(2,2),CQQ_qbswap(2,2),aq_qb0swap(2,2),aq_QQb0swap(2,2)
      complex(dp) :: DQQ_QQB(2,2),DQQ_qb(2,2)
      complex(dp) :: DQQ_QQBswap(2,2),DQQ_qbswap(2,2)
      complex(dp) :: EQQ_QQB(2,2),EQQ_qb(2,2)
      complex(dp) :: EQQ_QQBswap(2,2),EQQ_qbswap(2,2)
!!! CHECKING VARIABLES
      real(dp) :: sbar(mxpart,mxpart),sswapbar(mxpart,mxpart)
      complex(dp) :: xabar(mxpart,mxpart),xbbar(mxpart,mxpart)
      complex(dp) :: xaswapbar(mxpart,mxpart),xbswapbar(mxpart,mxpart)
      complex(dp) :: aQQ_qb0bar(2,2),aQQ_QQb0bar(2,2),aq_qb0bar(2,2),aq_QQb0bar(2,2)
      complex(dp) :: aQQ_qb1bar(2,2),aQQ_QQb1bar(2,2),aq_qb1bar(2,2),aq_QQb1bar(2,2)
      complex(dp) :: aQQ_qb0swapbar(2,2),aQQ_QQb0swapbar(2,2)
      complex(dp) :: aQQ_qb1swapbar(2,2),aQQ_QQb1swapbar(2,2)
      complex(dp) :: SQQ_qbbar(2,2),SQQ_QQbbar(2,2),RQQ_qbbar(2,2),RQQ_QQbbar(2,2),CQQ_QQbbar(2,2),CQQ_qbbar(2,2)
      complex(dp) :: SQQ_qbswapbar(2,2),SQQ_QQbswapbar(2,2),RQQ_qbswapbar(2,2),RQQ_QQbswapbar(2,2),
     & CQQ_QQbswapbar(2,2),CQQ_qbswapbar(2,2),aq_qb0swapbar(2,2),aq_QQb0swapbar(2,2)
      complex(dp) :: DQQ_QQBbar(2,2),DQQ_qbbar(2,2)
      complex(dp) :: DQQ_QQBswapbar(2,2),DQQ_qbswapbar(2,2)
      complex(dp) :: EQQ_QQBbar(2,2),EQQ_qbbar(2,2)
      complex(dp) :: EQQ_QQBswapbar(2,2),EQQ_qbswapbar(2,2)
      integer jj(5)
      
      jj(1)=j1
      jj(2)=j2
      jj(3)=j3
      jj(4)=j4
      jj(5)=j5

! loop over gluon helicity
      do hg=2,1,-1
      if (hg == 2) then
! for first pass (hel +) use as-is
        do j=1,5
        do k=1,5
        xa(j,k)=za(jj(j),jj(k))
        xb(j,k)=zb(jj(j),jj(k))
        ss(j,k)=s(jj(j),jj(k))
        enddo
        enddo
      else
! for second pass, interchange za and zb
        do j=1,5
        do k=1,5
        xa(j,k)=zb(jj(j),jj(k))
        xb(j,k)=za(jj(j),jj(k))
        ss(j,k)=s(jj(j),jj(k))
        enddo
        enddo
      endif

! spinors and invariants for the interchange q <-> Q
      do j=1,5
      do k=1,5
      xaswap(j,k)=xa(qQswap(j),qQswap(k))
      xbswap(j,k)=xb(qQswap(j),qQswap(k))
      sswap(j,k)=ss(qQswap(j),qQswap(k))
      enddo
      enddo

! fill tree-level amplitudes
      call fillaij0(xa,xb,aQQ_qb0,aQQ_QQb0)
      call fillaij0(xaswap,xbswap,aQQ_qb0swap,aQQ_QQb0swap)

! symmetry relations of Eqs. (3) and (4) at tree-level
      do hq=1,2
      do hQQ=1,2
      aq_qb0(hq,hQQ)=aQQ_QQb0swap(hQQ,hq)
      aq_QQb0(hq,hQQ)=aQQ_qb0swap(hQQ,hq)
      aq_qb0swap(hq,hQQ)=aQQ_QQb0(hQQ,hq)
      aq_QQb0swap(hq,hQQ)=aQQ_qb0(hQQ,hq)
      enddo
      enddo

! components of one-loop amplitude
      call fillSij(xa,xb,ss,aQQ_qb0,aQQ_QQb0,aq_qb0,aq_QQb0,SQQ_qb,SQQ_QQb)
      call fillRCij(xa,xb,ss,aQQ_qb0,aQQ_QQb0,RQQ_qb,RQQ_QQb,CQQ_QQb,CQQ_qb)
      call fillSij(xaswap,xbswap,sswap,aQQ_qb0swap,aQQ_QQb0swap,aq_qb0swap,aq_QQb0swap,
     & SQQ_qbswap,SQQ_QQbswap)
      call fillRCij(xaswap,xbswap,sswap,aQQ_qb0swap,aQQ_QQb0swap,
     & RQQ_qbswap,RQQ_QQbswap,CQQ_QQbswap,CQQ_qbswap)
      call fillDij(xa,xb,ss,aQQ_qb0,DQQ_qb,DQQ_QQb)
      call fillDij(xaswap,xbswap,sswap,aQQ_qb0swap,DQQ_qbswap,DQQ_QQbswap)
      call fillEij(xa,xb,ss,aQQ_qb0,aQQ_QQb0,aq_qb0,aq_QQb0,EQQ_qb,EQQ_QQb)
      call fillEij(xaswap,xbswap,sswap,aQQ_qb0swap,aQQ_QQb0swap,
     & aq_qb0swap,aq_QQb0swap,EQQ_qbswap,EQQ_QQbswap)
      
! decomposition of one-loop amplitude
      aQQ_qb1=SQQ_qb+RQQ_qb+CQQ_qb+DQQ_qb+EQQ_qb
      aQQ_QQb1=SQQ_QQb+RQQ_QQb+CQQ_QQb+DQQ_QQb+EQQ_QQb
      aQQ_qb1swap=SQQ_qbswap+RQQ_qbswap+CQQ_qbswap+DQQ_qbswap+EQQ_qbswap
      aQQ_QQb1swap=SQQ_QQbswap+RQQ_QQbswap+CQQ_QQbswap+DQQ_QQbswap+EQQ_QQbswap

! symmetry relations of Eqs. (3) and (4) at one-loop
      do hq=1,2
      do hQQ=1,2
      aq_qb1(hq,hQQ)=aQQ_QQb1swap(hQQ,hq)
      aq_QQb1(hq,hQQ)=aQQ_qb1swap(hQQ,hq)
      enddo
      enddo

! translation from dimensional reduction to tH-V scheme
      scheme=half*(2*CF+xn/6._dp+one/6._dp)
      aQQ_qb1 =aQQ_qb1  - scheme*aQQ_qb0
      aQQ_QQb1=aQQ_QQb1 - scheme*aQQ_QQb0
      aq_qb1  =aq_qb1   - scheme*aq_qb0
      aq_QQb1 =aq_QQb1  - scheme*aq_QQb0

! store in extended-helicity arrays
      aQQ_qb0_hel(:,:,hg)  = aQQ_qb0(:,:)
      aQQ_QQb0_hel(:,:,hg) = aQQ_QQb0(:,:)
      aq_qb0_hel(:,:,hg)   = aq_qb0(:,:)
      aq_QQb0_hel(:,:,hg)  = aq_QQb0(:,:)

      aQQ_qb1_hel(:,:,hg)  = aQQ_qb1(:,:)
      aQQ_QQb1_hel(:,:,hg) = aQQ_QQb1(:,:)
      aq_qb1_hel(:,:,hg)   = aq_qb1(:,:)
      aq_QQb1_hel(:,:,hg)  = aq_QQb1(:,:)

      enddo

      return
      end





!---       if (1 == 2) then
!--- ! BEGIN CHECKING BLOCK
!--- ! BEGIN CHECKING BLOCK
!--- ! BEGIN CHECKING BLOCK
!--- ! spinors and invariants for checking: bar swap
!---       do j=1,5
!---       do k=1,5
!---       xabar(j,k)=xa(bar(j),bar(k))
!---       xbbar(j,k)=xb(bar(j),bar(k))
!---       sbar(j,k)=s(bar(j),bar(k))
!---       enddo
!---       enddo
!---       do j=1,5
!---       do k=1,5
!---       xaswapbar(j,k)=xaswap(bar(j),bar(k))
!---       xbswapbar(j,k)=xbswap(bar(j),bar(k))
!---       sswapbar(j,k)=sswap(bar(j),bar(k))
!---       enddo
!---       enddo
!--- 
!--- ! fill tree-level amplitudes
!---       call fillaij0(xabar,xbbar,aQQ_qb0bar,aQQ_QQb0bar)
!---       call fillaij0(xaswapbar,xbswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar)
!--- 
!--- ! symmetry relations of Eqs. (3) and (4) at tree-level
!---       do hq=1,2
!---       do hQQ=1,2
!---       aq_qb0bar(hq,hQQ)=aQQ_QQb0swapbar(hQQ,hq)
!---       aq_QQb0bar(hq,hQQ)=aQQ_qb0swapbar(hQQ,hq)
!---       aq_qb0swapbar(hq,hQQ)=aQQ_QQb0bar(hQQ,hq)
!---       aq_QQb0swapbar(hq,hQQ)=aQQ_qb0bar(hQQ,hq)
!---       enddo
!---       enddo
!--- 
!--- ! components of one-loop amplitude
!---       call fillSij(xabar,xbbar,sbar,aQQ_qb0bar,aQQ_QQb0bar,aq_qb0bar,aq_QQb0bar,SQQ_qbbar,SQQ_QQbbar)
!---       call fillRCij(xabar,xbbar,sbar,aQQ_qb0bar,aQQ_QQb0bar,RQQ_qbbar,RQQ_QQbbar,CQQ_QQbbar,CQQ_qbbar)
!---       call fillSij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar,aq_qb0swapbar,aq_QQb0swapbar,
!---      & SQQ_qbswapbar,SQQ_QQbswapbar)
!---       call fillRCij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar,
!---      & RQQ_qbswapbar,RQQ_QQbswapbar,CQQ_QQbswapbar,CQQ_qbswapbar)
!---       call fillDij(xabar,xbbar,sbar,aQQ_qb0bar,DQQ_qbbar,DQQ_QQbbar)
!---       call fillDij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,DQQ_qbswapbar,DQQ_QQbswapbar)
!---       call fillEij(xabar,xbbar,sbar,aQQ_qb0bar,aQQ_QQb0bar,aq_qb0bar,aq_QQb0bar,EQQ_qbbar,EQQ_QQbbar)
!---       call fillEij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar,
!---      & aq_qb0swapbar,aq_QQb0swapbar,EQQ_qbswapbar,EQQ_QQbswapbar)
!---       
!--- ! decomposition of one-loop amplitude
!---       aQQ_qb1bar=SQQ_qbbar+RQQ_qbbar+CQQ_qbbar+DQQ_qbbar+EQQ_qbbar
!---       aQQ_QQb1bar=SQQ_QQbbar+RQQ_QQbbar+CQQ_QQbbar+DQQ_QQbbar+EQQ_QQbbar
!---       aQQ_qb1swapbar=SQQ_qbswapbar+RQQ_qbswapbar+CQQ_qbswapbar+DQQ_qbswapbar+EQQ_qbswapbar
!---       aQQ_QQb1swapbar=SQQ_QQbswapbar+RQQ_QQbswapbar+CQQ_QQbswapbar+DQQ_QQbswapbar+EQQ_QQbswapbar
!--- 
!---       write(6,*) '==== QQ_QQb'
!---       write(6,*) 'a0 check',aQQ_QQb0(1,1),-aQQ_QQb0bar(2,2)
!---       write(6,*) 'a0 check',aQQ_QQb0(2,1),-aQQ_QQb0bar(1,2)
!---       write(6,*) 'S check',SQQ_QQb(1,1),-SQQ_QQbbar(2,2)
!---       write(6,*) 'S check',SQQ_QQb(2,1),-SQQ_QQbbar(1,2)
!---       write(6,*) 'R check',RQQ_QQb(1,1),-RQQ_QQbbar(2,2)
!---       write(6,*) 'R check',RQQ_QQb(2,1),-RQQ_QQbbar(1,2)
!---       write(6,*) 'C check',CQQ_QQb(1,1),-CQQ_QQbbar(2,2)
!---       write(6,*) 'C check',CQQ_QQb(2,1),-CQQ_QQbbar(1,2)
!---       write(6,*) 'D check',DQQ_QQb(1,1),-DQQ_QQbbar(2,2)
!---       write(6,*) 'D check',DQQ_QQb(2,1),-DQQ_QQbbar(1,2)
!---       write(6,*) 'E check',EQQ_QQb(1,1),-EQQ_QQbbar(2,2)
!---       write(6,*) 'E check',EQQ_QQb(2,1),-EQQ_QQbbar(1,2)
!--- !      pause
!--- 
!--- ! spinors and invariants for checking: exch swap
!---       do j=1,5
!---       do k=1,5
!---       xabar(j,k)=xa(exch(j),exch(k))
!---       xbbar(j,k)=xb(exch(j),exch(k))
!---       sbar(j,k)=s(exch(j),exch(k))
!---       enddo
!---       enddo
!---       do j=1,5
!---       do k=1,5
!---       xaswapbar(j,k)=xaswap(exch(j),exch(k))
!---       xbswapbar(j,k)=xbswap(exch(j),exch(k))
!---       sswapbar(j,k)=sswap(exch(j),exch(k))
!---       enddo
!---       enddo
!--- 
!--- ! fill tree-level amplitudes
!---       call fillaij0(xabar,xbbar,aQQ_qb0bar,aQQ_QQb0bar)
!---       call fillaij0(xaswapbar,xbswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar)
!--- 
!--- ! symmetry relations of Eqs. (3) and (4) at tree-level
!---       do hq=1,2
!---       do hQQ=1,2
!---       aq_qb0bar(hq,hQQ)=aQQ_QQb0swapbar(hQQ,hq)
!---       aq_QQb0bar(hq,hQQ)=aQQ_qb0swapbar(hQQ,hq)
!---       aq_qb0swapbar(hq,hQQ)=aQQ_QQb0bar(hQQ,hq)
!---       aq_QQb0swapbar(hq,hQQ)=aQQ_qb0bar(hQQ,hq)
!---       enddo
!---       enddo
!--- 
!--- ! components of one-loop amplitude
!---       call fillSij(xabar,xbbar,sbar,aQQ_qb0bar,aQQ_QQb0bar,aq_qb0bar,aq_QQb0bar,SQQ_qbbar,SQQ_QQbbar)
!---       call fillRCij(xabar,xbbar,sbar,aQQ_qb0bar,aQQ_QQb0bar,RQQ_qbbar,RQQ_QQbbar,CQQ_QQbbar,CQQ_qbbar)
!---       call fillSij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar,aq_qb0swapbar,aq_QQb0swapbar,
!---      & SQQ_qbswapbar,SQQ_QQbswapbar)
!---       call fillRCij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar,
!---      & RQQ_qbswapbar,RQQ_QQbswapbar,CQQ_QQbswapbar,CQQ_qbswapbar)
!---       call fillDij(xabar,xbbar,sbar,aQQ_qb0bar,DQQ_qbbar,DQQ_QQbbar)
!---       call fillDij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,DQQ_qbswapbar,DQQ_QQbswapbar)
!---       call fillEij(xabar,xbbar,sbar,aQQ_qb0bar,aQQ_QQb0bar,aq_qb0bar,aq_QQb0bar,EQQ_qbbar,EQQ_QQbbar)
!---       call fillEij(xaswapbar,xbswapbar,sswapbar,aQQ_qb0swapbar,aQQ_QQb0swapbar,
!---      & aq_qb0swapbar,aq_QQb0swapbar,EQQ_qbswapbar,EQQ_QQbswapbar)
!---       
!--- ! decomposition of one-loop amplitude
!---       aQQ_qb1bar=SQQ_qbbar+RQQ_qbbar+CQQ_qbbar+DQQ_qbbar+EQQ_qbbar
!---       aQQ_QQb1bar=SQQ_QQbbar+RQQ_QQbbar+CQQ_QQbbar+DQQ_QQbbar+EQQ_QQbbar
!---       aQQ_qb1swapbar=SQQ_qbswapbar+RQQ_qbswapbar+CQQ_qbswapbar+DQQ_qbswapbar+EQQ_qbswapbar
!---       aQQ_QQb1swapbar=SQQ_QQbswapbar+RQQ_QQbswapbar+CQQ_QQbswapbar+DQQ_QQbswapbar+EQQ_QQbswapbar
!---       
!---       write(6,*) '==== QQ_qb'
!---       write(6,*) 'a0 check',aQQ_qb0(1,1),-aQQ_qb0bar(2,2)
!--- !      write(6,*) 'a0 check',aQQ_qb0(2,1),-aQQ_qb0bar(1,2)
!---       write(6,*) 'S check',SQQ_qb(1,1),-SQQ_qbbar(2,2)
!--- !      write(6,*) 'S check',SQQ_qb(2,1),-SQQ_qbbar(1,2)
!---       write(6,*) 'R check',RQQ_qb(1,1),-RQQ_qbbar(2,2)
!--- !      write(6,*) 'R check',RQQ_qb(2,1),-RQQ_qbbar(1,2)
!--- !      write(6,*) 'C check',CQQ_qb(1,1),-CQQ_qbbar(2,2)
!--- !      write(6,*) 'C check',CQQ_qb(2,1),-CQQ_qbbar(1,2)
!---       write(6,*) 'C+D check',CQQ_qb(1,1)+DQQ_qb(1,1),-(CQQ_qbbar(2,2)+DQQ_qbbar(2,2))
!--- !      write(6,*) 'D check',DQQ_qb(2,1),-DQQ_qbbar(1,2)
!---       write(6,*) 'E check',EQQ_qb(1,1),-EQQ_qbbar(2,2)
!--- !      write(6,*) 'E check',EQQ_qb(2,1),-EQQ_qbbar(1,2)
!---       pause
!--- ! END OF CHECKING BLOCK
!--- ! END OF CHECKING BLOCK
!--- ! END OF CHECKING BLOCK
!---       endif
