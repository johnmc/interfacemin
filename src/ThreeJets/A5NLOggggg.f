      function A5NLOggggg()
! Implementation of one-loop x tree interference for:
!
! g + g -> g + g + g
!
! Result taken from Eq. (11) of:
!
! 'One loop corrections to five gluon amplitudes'
!  Zvi Bern, Lance J. Dixon, David A. Kosower
!  Published in: Phys.Rev.Lett. 70 (1993) 2677-2680
!  hep-ph/930228
!
! See also:
! 'Color decomposition of one-loop amplitudes in gauge theories'
!  Zvi Bern, David A. Kosower, Nuclear Physics B362 (1991) 389-448
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'qcdcouple.f'
      include 'zprods_com.f'
! list of permuation of S5/Z5
      integer, parameter, dimension(5,24)  :: S5Z5(5,24) = reshape([
     & 1, 2, 3, 4, 5,    1, 2, 4, 3, 5,    1, 3, 2, 4, 5,    1, 3, 4, 2, 5,
     & 1, 4, 2, 3, 5,    1, 4, 3, 2, 5,    2, 1, 3, 4, 5,    2, 1, 4, 3, 5,
     & 2, 3, 1, 4, 5,    2, 3, 4, 1, 5,    2, 4, 1, 3, 5,    2, 4, 3, 1, 5,
     & 3, 1, 2, 4, 5,    3, 1, 4, 2, 5,    3, 2, 1, 4, 5,    3, 2, 4, 1, 5,
     & 3, 4, 1, 2, 5,    3, 4, 2, 1, 5,    4, 1, 2, 3, 5,    4, 1, 3, 2, 5,
     & 4, 2, 1, 3, 5,    4, 2, 3, 1, 5,    4, 3, 1, 2, 5,    4, 3, 2, 1, 5 ], [5,24])
      integer ro,pp,ih,h1,h2,h3,h4,h5
! pointer to action of h (in H5) on p in P(5 3)
      integer, parameter, dimension(6,10)  :: hdotp = reshape([
     & 1, 17, 13,  7, 15, 18,  3, 11,  7, 13,  9, 12,  5,  9,  8, 19, 11, 10, 10, 22, 18,  1,  4,  5,  9,
     &  5,  1, 15,  3,  6, 11,  3,  2, 21,  5,  4,  4, 20, 17,  7, 10, 11, 17,  1,  4, 23,  6,  2,  2, 19,
     &  11, 13, 16, 17,  1, 13,  9, 19, 22, 23], [6,10])
! pointer to action of r=(24135) on rho (in S5Z5)
      integer, parameter, dimension(24) :: rdotrho = [
     & 11, 9, 17, 15, 23, 21, 5, 3, 18, 13, 24, 19, 6, 1, 12, 7, 22, 20, 4, 2, 10, 8, 16, 14 ]
      complex(dp) :: A50(2,2,2,2,2),A51one(2,2,2,2,2),A51half(2,2,2,2,2),
     & A5tree(24,2,2,2,2,2),A51(24,2,2,2,2,2),A51gluon(24,2,2,2,2,2),A53(10,2,2,2,2,2)
      real(dp) :: A5NLOggggg

! Set up basic amplitudes
      do ro=1,24
        call A5gfill(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),za,zb,A50,A51half,A51one)
        A5tree(ro,:,:,:,:,:)=A50(:,:,:,:,:)
        A51gluon(ro,:,:,:,:,:)=A51one(:,:,:,:,:)
        A51(ro,:,:,:,:,:)=A51one(:,:,:,:,:)+float(nflav)/xn*A51half(:,:,:,:,:)
      enddo

! Identity for A53 taken from Eqs. (6.11) and (7.5) of
! 'Color decomposition of one-loop amplitudes in gauge theories'
!  Zvi Bern, David A. Kosower, Nuclear Physics B362 (1991) 389-448
!
! id,A53(j4?,j5?,j1?,j2?,j3?)=
!  + A51(j1,j2,j3,j4,j5) + A51(j1,j2,j4,j3,j5) + A51(j1,j4,j2,j3,j5)
!  + A51(j4,j1,j2,j3,j5) + A51(j2,j3,j1,j4,j5) + A51(j2,j3,j4,j1,j5)
!  + A51(j2,j4,j3,j1,j5) + A51(j4,j2,j3,j1,j5) + A51(j3,j1,j2,j4,j5)
!  + A51(j3,j1,j4,j2,j5) + A51(j3,j4,j1,j2,j5) + A51(j4,j3,j1,j2,j5);

! A53(12345)
      A53(1,:,:,:,:,:) = 
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(3,:,:,:,:,:) + A51gluon(4,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(9,:,:,:,:,:) + A51gluon(10,:,:,:,:,:)
     &  + A51gluon(13,:,:,:,:,:) + A51gluon(14,:,:,:,:,:) + A51gluon(15,:,:,:,:,:)
     &  + A51gluon(16,:,:,:,:,:) + A51gluon(17,:,:,:,:,:) + A51gluon(18,:,:,:,:,:)

! A53(13245)
      A53(2,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(3,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(8,:,:,:,:,:) + A51gluon(9,:,:,:,:,:)
     &  + A51gluon(10,:,:,:,:,:) + A51gluon(11,:,:,:,:,:) + A51gluon(12,:,:,:,:,:)
     &  + A51gluon(13,:,:,:,:,:) + A51gluon(15,:,:,:,:,:) + A51gluon(16,:,:,:,:,:)

! A53(14235)
      A53(3,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(5,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(8,:,:,:,:,:) + A51gluon(9,:,:,:,:,:)
     &  + A51gluon(10,:,:,:,:,:) + A51gluon(11,:,:,:,:,:) + A51gluon(12,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(21,:,:,:,:,:) + A51gluon(22,:,:,:,:,:)

! A53(15234)
      A53(4,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(4,:,:,:,:,:) + A51gluon(5,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(9,:,:,:,:,:) + A51gluon(10,:,:,:,:,:)
     &  + A51gluon(14,:,:,:,:,:) + A51gluon(17,:,:,:,:,:) + A51gluon(18,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(21,:,:,:,:,:) + A51gluon(22,:,:,:,:,:)

! A53(23145)
      A53(5,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(3,:,:,:,:,:)
     &  + A51gluon(4,:,:,:,:,:) + A51gluon(5,:,:,:,:,:) + A51gluon(6,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(8,:,:,:,:,:) + A51gluon(9,:,:,:,:,:)
     &  + A51gluon(13,:,:,:,:,:) + A51gluon(14,:,:,:,:,:) + A51gluon(15,:,:,:,:,:)

! A53(24135)
      A53(6,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(3,:,:,:,:,:)
     &  + A51gluon(4,:,:,:,:,:) + A51gluon(5,:,:,:,:,:) + A51gluon(6,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(8,:,:,:,:,:) + A51gluon(11,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(20,:,:,:,:,:) + A51gluon(21,:,:,:,:,:)

! A53(25134)
      A53(7,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(3,:,:,:,:,:) + A51gluon(4,:,:,:,:,:)
     &  + A51gluon(7,:,:,:,:,:) + A51gluon(10,:,:,:,:,:) + A51gluon(11,:,:,:,:,:)
     &  + A51gluon(16,:,:,:,:,:) + A51gluon(17,:,:,:,:,:) + A51gluon(18,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(20,:,:,:,:,:) + A51gluon(21,:,:,:,:,:)

! A53(34125)
      A53(8,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(3,:,:,:,:,:)
     &  + A51gluon(4,:,:,:,:,:) + A51gluon(5,:,:,:,:,:) + A51gluon(6,:,:,:,:,:)
     &  + A51gluon(13,:,:,:,:,:) + A51gluon(14,:,:,:,:,:) + A51gluon(17,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(20,:,:,:,:,:) + A51gluon(23,:,:,:,:,:)

! A53(35124)
      A53(9,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(3,:,:,:,:,:)
     &  + A51gluon(10,:,:,:,:,:) + A51gluon(11,:,:,:,:,:) + A51gluon(12,:,:,:,:,:)
     &  + A51gluon(13,:,:,:,:,:) + A51gluon(16,:,:,:,:,:) + A51gluon(17,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(20,:,:,:,:,:) + A51gluon(23,:,:,:,:,:)

! A53(45123)
      A53(10,:,:,:,:,:) =
     &  + A51gluon(1,:,:,:,:,:) + A51gluon(2,:,:,:,:,:) + A51gluon(5,:,:,:,:,:)
     &  + A51gluon(9,:,:,:,:,:) + A51gluon(10,:,:,:,:,:) + A51gluon(12,:,:,:,:,:)
     &  + A51gluon(13,:,:,:,:,:) + A51gluon(14,:,:,:,:,:) + A51gluon(17,:,:,:,:,:)
     &  + A51gluon(19,:,:,:,:,:) + A51gluon(22,:,:,:,:,:) + A51gluon(23,:,:,:,:,:)
      
! Perform sum according to Eq. (11)
      A5NLOggggg=0d0
      do h1=1,2
      do h2=1,2
      do h3=1,2
      do h4=1,2
      do h5=1,2
        do ro=1,24
        A5NLOggggg=A5NLOggggg
     &   +real(conjg(A5tree(ro,h1,h2,h3,h4,h5))*A51(ro,h1,h2,h3,h4,h5),kind=dp)

     &   +real(conjg(A5tree(rdotrho(ro),h1,h2,h3,h4,h5))*A51(ro,h1,h2,h3,h4,h5)
     &        -conjg(A5tree(ro,h1,h2,h3,h4,h5))*A51(rdotrho(ro),h1,h2,h3,h4,h5),kind=dp)/xn**2
        enddo
        do pp=1,10
        do ih=1,6
          A5NLOggggg=A5NLOggggg
     &     +real(conjg(A5tree(hdotp(ih,pp),h1,h2,h3,h4,h5))*A53(pp,h1,h2,h3,h4,h5),kind=dp)*two/xn**2
        enddo
        enddo

      enddo
      enddo
      enddo
      enddo
      enddo

! Remember that we have removed a factor of 16*pi^2 from the amplitudes
      A5NLOggggg=two*gsq**3*ason4pi*xn**4*V*A5NLOggggg
      
      return
      end



!---      function getS5Z5(sig,i1in,i2in,i3in,i4in,i5in)
!---! returns index of sig.(i1 i2 i3 i4 i5)
!---      implicit none
!---      include 'types.f'
!---! list of permuation of S5/Z5
!---      integer, parameter, dimension(5,24)  :: S5Z5(5,24) = reshape([
!---     & 1, 2, 3, 4, 5,    1, 2, 4, 3, 5,    1, 3, 2, 4, 5,    1, 3, 4, 2, 5,
!---     & 1, 4, 2, 3, 5,    1, 4, 3, 2, 5,    2, 1, 3, 4, 5,    2, 1, 4, 3, 5,
!---     & 2, 3, 1, 4, 5,    2, 3, 4, 1, 5,    2, 4, 1, 3, 5,    2, 4, 3, 1, 5,
!---     & 3, 1, 2, 4, 5,    3, 1, 4, 2, 5,    3, 2, 1, 4, 5,    3, 2, 4, 1, 5,
!---     & 3, 4, 1, 2, 5,    3, 4, 2, 1, 5,    4, 1, 2, 3, 5,    4, 1, 3, 2, 5,
!---     & 4, 2, 1, 3, 5,    4, 2, 3, 1, 5,    4, 3, 1, 2, 5,    4, 3, 2, 1, 5 ], [5,24])
!---      integer getS5Z5,sig,i1,i2,i3,i4,i5,i1in,i2in,i3in,i4in,i5in,ro,isave
!---      
!---      getS5Z5=0
!---   
!---      i1=i1in
!---      i2=i2in
!---      i3=i3in
!---      i4=i4in
!---      i5=i5in
!---   77 continue   
!---      if (i5 /= 5) then
!---        isave=i1
!---        i1=i2
!---        i2=i3
!---        i3=i4
!---        i4=i5       
!---        i5=isave
!---        goto 77
!---      endif
!---      
!---      do ro=1,24
!---        if ((S5Z5(i1,sig) == S5Z5(1,ro)) .and. (S5Z5(i2,sig) == S5Z5(2,ro)) .and. (S5Z5(i3,sig) == S5Z5(3,ro))
!---     &      .and. (S5Z5(i4,sig) == S5Z5(4,ro)) .and. (S5Z5(i5,sig) == S5Z5(5,ro))) then
!---           getS5Z5=ro
!---           exit
!---        endif
!---      enddo
!---      
!---      if (getS5Z5 == 0) then
!---        write(6,*) 'error in getS5Z5: ',sig,i1,i2,i3,i4,i5
!---        stop
!---      endif
!---      
!---      return
!---      end
      



!---      program permute
!---! computes hdotp
!---      implicit none
!---      integer ii,ih,iarr(5),hdotp(5),getS5Z5
!---      
!---      do ii=1,10
!---      
!---      if (ii==1) then
!---        iarr=[1,2,3,4,5]
!---      endif
!---      if (ii==2) then
!---        iarr=[1,3,2,4,5]
!---      endif
!---      if (ii==3) then
!---        iarr=[1,4,2,3,5]
!---      endif
!---      if (ii==4) then
!---        iarr=[1,5,2,3,4]
!---      endif
!---      if (ii==5) then
!---        iarr=[2,3,1,4,5]
!---      endif
!---      if (ii==6) then
!---        iarr=[2,4,1,3,5]
!---      endif
!---      if (ii==7) then
!---        iarr=[2,5,1,3,4]
!---      endif
!---      if (ii==8) then
!---        iarr=[3,4,1,2,5]
!---      endif
!---      if (ii==9) then
!---        iarr=[3,5,1,2,4]
!---      endif
!---      if (ii==10) then
!---        iarr=[4,5,1,2,3]
!---      endif
!---      
!---      do ih=1,6
!---      
!---      if (ih == 1) then
!---        hdotp(1)=iarr(1)
!---        hdotp(2)=iarr(2)
!---        hdotp(3)=iarr(3)
!---        hdotp(4)=iarr(4)
!---        hdotp(5)=iarr(5)
!---      endif
!---      if (ih == 2) then
!---        hdotp(1)=iarr(3)
!---        hdotp(2)=iarr(4)
!---        hdotp(3)=iarr(1)
!---        hdotp(4)=iarr(2)
!---        hdotp(5)=iarr(5)
!---      endif
!---      if (ih == 3) then
!---        hdotp(1)=iarr(3)
!---        hdotp(2)=iarr(1)
!---        hdotp(3)=iarr(2)
!---        hdotp(4)=iarr(4)
!---        hdotp(5)=iarr(5)
!---      endif
!---      if (ih == 4) then
!---        hdotp(1)=iarr(2)
!---        hdotp(2)=iarr(1)
!---        hdotp(3)=iarr(3)
!---        hdotp(4)=iarr(4)
!---        hdotp(5)=iarr(5)
!---      endif
!---      if (ih == 5) then
!---        hdotp(1)=iarr(3)
!---        hdotp(2)=iarr(2)
!---        hdotp(3)=iarr(1)
!---        hdotp(4)=iarr(4)
!---        hdotp(5)=iarr(5)
!---      endif
!---      if (ih == 6) then
!---        hdotp(1)=iarr(3)
!---        hdotp(2)=iarr(4)
!---        hdotp(3)=iarr(2)
!---        hdotp(4)=iarr(1)
!---        hdotp(5)=iarr(5)
!---      endif
!---      
!---      write(6,*) ii,ih,getS5Z5(1,hdotp(1),hdotp(2),hdotp(3),hdotp(4),hdotp(5))
!---      
!---      enddo
!---      
!---      enddo
!---      
!---      stop
!---      end
