      function A5NLOqbqggg(j1,j2,j3,j4,j5)
! Implementation of one-loop x tree interference for:
!
! 0 -> q~(j1) + q(j2) + g(j3) + g(j4) + g(j5)
!
! Result taken from Eq. (2.11) of:
!
! 'One-Loop Corrections to Two-Quark Three-Gluon Amplitudes'
!  Zvi Bern, Lance J. Dixon, David A. Kosower
!  Published in: Nucl.Phys.B437:259-304,1995
!  hep-ph/9409393
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'b0.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'qcdcouple.f'
      include 'zprods_com.f'
      real(dp) :: A5NLOqbqggg,A5LO
      integer j1,j2,j3,j4,j5,sig,hq,h3,h4,h5
      integer, parameter :: g345=1, g354=2, g435=3, g453=4, g534=5, g543=6
c--- action of permutations on these indices
      integer, parameter:: sig354(6) = [ g354, g345, g453, g435, g543, g534]
      integer, parameter:: sig435(6) = [ g435, g534, g345, g543, g354, g453]
      integer, parameter:: sig453(6) = [ g453, g543, g354, g534, g345, g435]
      integer, parameter:: sig534(6) = [ g534, g435, g543, g345, g453, g354]
      integer, parameter:: sig543(6) = [ g543, g453, g534, g354, g435, g345]
! contains helicity amplitudes labelled by (qb, g, g, g)
      complex(dp) :: miA5tree_QBQggg(2,2,2,2),AL_QBQggg(2,2,2,2),
     & ASUSY_QBQggg(2,2,2,2),Af_QBQggg(2,2,2,2),As_QBQggg(2,2,2,2)
      complex(dp) :: miA5tree_QBgQgg(2,2,2,2),AL_QBgQgg(2,2,2,2),
     & ASUSY_QBgQgg(2,2,2,2),Af_QBgQgg(2,2,2,2),As_QBgQgg(2,2,2,2)
! contains helicity amplitudes labelled by (iperm, qb, g, g, g)
      complex(dp) :: miA5tree_QBQggg_perm(6,2,2,2,2),AL_QBQggg_perm(6,2,2,2,2),
     & ASUSY_QBQggg_perm(6,2,2,2,2),Af_QBQggg_perm(6,2,2,2,2),As_QBQggg_perm(6,2,2,2,2)
      complex(dp) :: miA5tree_QBgQgg_perm(6,2,2,2,2),AL_QBgQgg_perm(6,2,2,2,2),
     & ASUSY_QBgQgg_perm(6,2,2,2,2),Af_QBgQgg_perm(6,2,2,2,2),As_QBgQgg_perm(6,2,2,2,2)
      complex(dp) :: A51(6,2,2,2,2),A53(6,2,2,2,2),A54(6,2,2,2,2),A5loop(6,2,2,2,2)
      complex(dp) :: S3sum(2,2,2,2),Z3sum(2,2,2,2)

! compute all orderings and permute helicity labellings appropriately
      call fillamps_QBQggg(j1,j2,j3,j4,j5,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
      call fillperm(g345,3,4,5,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)

      call fillamps_QBQggg(j1,j2,j3,j5,j4,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
      call fillperm(g354,3,5,4,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)

      call fillamps_QBQggg(j1,j2,j4,j3,j5,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
      call fillperm(g435,4,3,5,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)

      call fillamps_QBQggg(j1,j2,j4,j5,j3,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
      call fillperm(g453,4,5,3,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)

      call fillamps_QBQggg(j1,j2,j5,j3,j4,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
      call fillperm(g534,5,3,4,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)

      call fillamps_QBQggg(j1,j2,j5,j4,j3,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
      call fillperm(g543,5,4,3,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)

! Leading partial amplitude, Eq. (4.2) with ns = 0
      do sig=1,6
      A51(sig,:,:,:,:) =
     & (1._dp+1._dp/xn**2)*AL_QBQggg_perm(sig,:,:,:,:)-ASUSY_QBQggg_perm(sig,:,:,:,:)/xn**2
     & -(float(nflav)/xn+1._dp/xn**2)*Af_QBQggg_perm(sig,:,:,:,:)
     & +(-float(nflav)/xn-1._dp/xn**2)*As_QBQggg_perm(sig,:,:,:,:)
      enddo

! Eq. (4.9) - A5;3, with ns = 0
      S3sum(:,:,:,:)=
     & +AL_QBQggg_perm(1,:,:,:,:)+AL_QBgQgg_perm(1,:,:,:,:)
     & +AL_QBQggg_perm(2,:,:,:,:)+AL_QBgQgg_perm(2,:,:,:,:)
     & +AL_QBQggg_perm(3,:,:,:,:)+AL_QBgQgg_perm(3,:,:,:,:)
     & +AL_QBQggg_perm(4,:,:,:,:)+AL_QBgQgg_perm(4,:,:,:,:)
     & +AL_QBQggg_perm(5,:,:,:,:)+AL_QBgQgg_perm(5,:,:,:,:)
     & +AL_QBQggg_perm(6,:,:,:,:)+AL_QBgQgg_perm(6,:,:,:,:)

      A53(g453,:,:,:,:)=S3sum(:,:,:,:)
     & -ASUSY_QBgQgg_perm(g345,:,:,:,:)-ASUSY_QBgQgg_perm(g354,:,:,:,:)
      A53(g534,:,:,:,:)=S3sum(:,:,:,:)
     & -ASUSY_QBgQgg_perm(g453,:,:,:,:)-ASUSY_QBgQgg_perm(g435,:,:,:,:)
      A53(g345,:,:,:,:)=S3sum(:,:,:,:)
     & -ASUSY_QBgQgg_perm(g534,:,:,:,:)-ASUSY_QBgQgg_perm(g543,:,:,:,:)

      A53(g435,:,:,:,:)=S3sum(:,:,:,:)
     & -ASUSY_QBgQgg_perm(g543,:,:,:,:)-ASUSY_QBgQgg_perm(g534,:,:,:,:)
      A53(g543,:,:,:,:)=S3sum(:,:,:,:)
     & -ASUSY_QBgQgg_perm(g354,:,:,:,:)-ASUSY_QBgQgg_perm(g345,:,:,:,:)
      A53(g354,:,:,:,:)=S3sum(:,:,:,:)
     & -ASUSY_QBgQgg_perm(g435,:,:,:,:)-ASUSY_QBgQgg_perm(g453,:,:,:,:)

! Eq. (4.9) - A5;4, with ns = 0
      A54(g345,:,:,:,:)=-S3sum(:,:,:,:)
     & +ASUSY_QBQggg_perm(g345,:,:,:,:)+ASUSY_QBgQgg_perm(g345,:,:,:,:)
     & +ASUSY_QBQggg_perm(g453,:,:,:,:)+ASUSY_QBgQgg_perm(g453,:,:,:,:)
     & +ASUSY_QBQggg_perm(g534,:,:,:,:)+ASUSY_QBgQgg_perm(g534,:,:,:,:)
     &+(float(nflav)/xn+one)*(
     & +As_QBQggg_perm(g345,:,:,:,:)+As_QBgQgg_perm(g345,:,:,:,:)
     & +As_QBQggg_perm(g453,:,:,:,:)+As_QBgQgg_perm(g453,:,:,:,:)
     & +As_QBQggg_perm(g534,:,:,:,:)+As_QBgQgg_perm(g534,:,:,:,:)
     & +Af_QBQggg_perm(g345,:,:,:,:)+Af_QBgQgg_perm(g345,:,:,:,:)
     & +Af_QBQggg_perm(g453,:,:,:,:)+Af_QBgQgg_perm(g453,:,:,:,:)
     & +Af_QBQggg_perm(g534,:,:,:,:)+Af_QBgQgg_perm(g534,:,:,:,:)
     & )

      A54(g354,:,:,:,:)=-S3sum(:,:,:,:)
     & +ASUSY_QBQggg_perm(g354,:,:,:,:)+ASUSY_QBgQgg_perm(g354,:,:,:,:)
     & +ASUSY_QBQggg_perm(g543,:,:,:,:)+ASUSY_QBgQgg_perm(g543,:,:,:,:)
     & +ASUSY_QBQggg_perm(g435,:,:,:,:)+ASUSY_QBgQgg_perm(g435,:,:,:,:)
     &+(float(nflav)/xn+one)*(
     & +As_QBQggg_perm(g354,:,:,:,:)+As_QBgQgg_perm(g354,:,:,:,:)
     & +As_QBQggg_perm(g543,:,:,:,:)+As_QBgQgg_perm(g543,:,:,:,:)
     & +As_QBQggg_perm(g435,:,:,:,:)+As_QBgQgg_perm(g435,:,:,:,:)
     & +Af_QBQggg_perm(g354,:,:,:,:)+Af_QBgQgg_perm(g354,:,:,:,:)
     & +Af_QBQggg_perm(g543,:,:,:,:)+Af_QBgQgg_perm(g543,:,:,:,:)
     & +Af_QBQggg_perm(g435,:,:,:,:)+Af_QBgQgg_perm(g435,:,:,:,:)
     & )
     
      A54(g453,:,:,:,:)=A54(g345,:,:,:,:)
      A54(g534,:,:,:,:)=A54(g345,:,:,:,:)

      A54(g543,:,:,:,:)=A54(g354,:,:,:,:)
      A54(g435,:,:,:,:)=A54(g354,:,:,:,:)

! Translation to MSbar scheme, per Eq. (5.3),
! and passage from FDH to tH-V, per Eq. (5.5)
      A51(:,:,:,:,:)=A51(:,:,:,:,:)
     & -(epinv*b0+(one-one/xn**2)/two)*miA5tree_QBQggg_perm(:,:,:,:,:)

! Assemble 1-loop amplitude that enters interference according to Eq. (2.11)
      do sig=1,6
      A5loop(sig,:,:,:,:) =
     & (xn**2-one)**2*A51(sig,:,:,:,:)
     & -(xn**2-one)*(A51(sig435(sig),:,:,:,:)+A51(sig354(sig),:,:,:,:)
     &              -A53(sig453(sig),:,:,:,:)-A53(sig,:,:,:,:))
     & +(xn**2+one)*A51(sig543(sig),:,:,:,:)
     & +(xn**2-two)*A54(sig,:,:,:,:)
     & +A51(sig453(sig),:,:,:,:)+A51(sig534(sig),:,:,:,:)
     & -A53(sig534(sig),:,:,:,:)-2*A54(sig543(sig),:,:,:,:)
      enddo

! Perform color and helicity sum
      A5NLOqbqggg=zip
      A5LO=zip
      do hq=1,2
      do h3=1,2
      do h4=1,2
      do h5=1,2
      do sig=1,6
      A5NLOqbqggg=A5NLOqbqggg+real(
     & conjg(miA5tree_QBQggg_perm(sig,hq,h3,h4,h5))*A5loop(sig,hq,h3,h4,h5),kind=dp)

! Tree-level: structure like A51 at NLO
!      A5LO=A5LO+real(conjg(miA5tree_QBQggg_perm(sig,hq,h3,h4,h5))*(
!     &  (xn**2-one)**2*miA5tree_QBQggg_perm(sig,hq,h3,h4,h5)
!     & -(xn**2-one)*(miA5tree_QBQggg_perm(sig435(sig),hq,h3,h4,h5)
!     &              +miA5tree_QBQggg_perm(sig354(sig),hq,h3,h4,h5))
!     & +(xn**2+one)*miA5tree_QBQggg_perm(sig543(sig),hq,h3,h4,h5)
!     & +miA5tree_QBQggg_perm(sig453(sig),hq,h3,h4,h5)+miA5tree_QBQggg_perm(sig534(sig),hq,h3,h4,h5)
!     & ),kind=dp)
      enddo
      enddo
      enddo
      enddo
      enddo
      
      A5NLOqbqggg=A5NLOqbqggg*2*gsq**3*V/xn*ason4pi

! Tree-level result: same as NLO but with overall 1/N
!      A5NLOqbqggg=A5LO*gsq**3*(V/xn**2)

      return
      end
      
      
      subroutine fillperm(sig,j3,j4,j5,
     & miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     & miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg,
     & miA5tree_QBQggg_perm,AL_QBQggg_perm,ASUSY_QBQggg_perm,Af_QBQggg_perm,As_QBQggg_perm,
     & miA5tree_QBgQgg_perm,AL_QBgQgg_perm,ASUSY_QBgQgg_perm,Af_QBgQgg_perm,As_QBgQgg_perm)
c--- fills permuation array with corresponding entries, permuting helicities as appropriate
      implicit none
      include 'types.f'
      integer sig,j3,j4,j5,h3,h4,h5,hh(3:5)
      complex(dp) :: miA5tree_QBQggg(2,2,2,2),AL_QBQggg(2,2,2,2),
     & ASUSY_QBQggg(2,2,2,2),Af_QBQggg(2,2,2,2),As_QBQggg(2,2,2,2)
      complex(dp) :: miA5tree_QBgQgg(2,2,2,2),AL_QBgQgg(2,2,2,2),
     & ASUSY_QBgQgg(2,2,2,2),Af_QBgQgg(2,2,2,2),As_QBgQgg(2,2,2,2)
      complex(dp) :: miA5tree_QBQggg_perm(6,2,2,2,2),AL_QBQggg_perm(6,2,2,2,2),
     & ASUSY_QBQggg_perm(6,2,2,2,2),Af_QBQggg_perm(6,2,2,2,2),As_QBQggg_perm(6,2,2,2,2)
      complex(dp) :: miA5tree_QBgQgg_perm(6,2,2,2,2),AL_QBgQgg_perm(6,2,2,2,2),
     & ASUSY_QBgQgg_perm(6,2,2,2,2),Af_QBgQgg_perm(6,2,2,2,2),As_QBgQgg_perm(6,2,2,2,2)
     
      do h3=1,2
      do h4=1,2
      do h5=1,2
      hh(j3)=h3
      hh(j4)=h4
      hh(j5)=h5

      miA5tree_QBQggg_perm(sig,:,hh(3),hh(4),hh(5))=miA5tree_QBQggg(:,h3,h4,h5)
      AL_QBQggg_perm(sig,:,hh(3),hh(4),hh(5))=AL_QBQggg(:,h3,h4,h5)
      ASUSY_QBQggg_perm(sig,:,hh(3),hh(4),hh(5))=ASUSY_QBQggg(:,h3,h4,h5)
      Af_QBQggg_perm(sig,:,hh(3),hh(4),hh(5))=Af_QBQggg(:,h3,h4,h5)
      As_QBQggg_perm(sig,:,hh(3),hh(4),hh(5))=As_QBQggg(:,h3,h4,h5)

      miA5tree_QBgQgg_perm(sig,:,hh(3),hh(4),hh(5))=miA5tree_QBgQgg(:,h3,h4,h5)
      AL_QBgQgg_perm(sig,:,hh(3),hh(4),hh(5))=AL_QBgQgg(:,h3,h4,h5)
      ASUSY_QBgQgg_perm(sig,:,hh(3),hh(4),hh(5))=ASUSY_QBgQgg(:,h3,h4,h5)
      Af_QBgQgg_perm(sig,:,hh(3),hh(4),hh(5))=Af_QBgQgg(:,h3,h4,h5)
      As_QBgQgg_perm(sig,:,hh(3),hh(4),hh(5))=As_QBgQgg(:,h3,h4,h5)

      enddo
      enddo
      enddo
      
      return
      end


