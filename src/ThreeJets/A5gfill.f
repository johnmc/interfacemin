!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!
! This routine built on the same structure as Aboxfill

      subroutine A5gfill(j1,j2,j3,j4,j5,za,zb,miA5tree,A51half,A51one)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      include 'zprods_decl.f'
      complex(dp)::miA5t,A51h,A51o,
     & miA5tree(2,2,2,2,2),A51half(2,2,2,2,2),A51one(2,2,2,2,2)
      integer:: h(5),j1,j2,j3,j4,j5

!      Abox(2,2,2,2,2)=A51ppppp(j1,j2,j3,j4,j5,za,zb)
!      Abox(1,1,1,1,1)=A51ppppp(j1,j2,j3,j4,j5,zb,za)

!      call helfill(1,2,2,2,2,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j1,j2,j3,j4,j5,za,zb)
!      call helfill(2,1,2,2,2,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j2,j3,j4,j5,j1,za,zb)
!      call helfill(2,2,1,2,2,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j3,j4,j5,j1,j2,za,zb)
!      call helfill(2,2,2,1,2,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j4,j5,j1,j2,j3,za,zb)
!      call helfill(2,2,2,2,1,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j5,j1,j2,j3,j4,za,zb)

!      call helfill(2,1,1,1,1,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j1,j2,j3,j4,j5,zb,za)
!      call helfill(1,2,1,1,1,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j2,j3,j4,j5,j1,zb,za)
!      call helfill(1,1,2,1,1,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j3,j4,j5,j1,j2,zb,za)
!      call helfill(1,1,1,2,1,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j4,j5,j1,j2,j3,zb,za)
!      call helfill(1,1,1,1,2,j1,j2,j3,j4,j5,h)
!      Abox(h(1),h(2),h(3),h(4),h(5))=A51mpppp(j5,j1,j2,j3,j4,zb,za)

! Only fill amplitudes with non-vanishing tree-level entries,
! so no calls to ppppp or mpppp

      miA5tree(:,:,:,:,:)=czip
      A51half(:,:,:,:,:)=czip
      A51one(:,:,:,:,:)=czip

      call helfill(1,1,2,2,2,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j1,j2,j3,j4,j5,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,1,1,2,2,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j2,j3,j4,j5,j1,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,2,1,1,2,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j3,j4,j5,j1,j2,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,2,2,1,1,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j4,j5,j1,j2,j3,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,2,2,2,1,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j5,j1,j2,j3,j4,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o

      call helfill(2,2,1,1,1,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j1,j2,j3,j4,j5,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,2,2,1,1,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j2,j3,j4,j5,j1,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,1,2,2,1,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j3,j4,j5,j1,j2,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,1,1,2,2,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j4,j5,j1,j2,j3,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,1,1,1,2,j1,j2,j3,j4,j5,h)
      call A51mmppp5g(j5,j1,j2,j3,j4,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o

      call helfill(1,2,1,2,2,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j1,j2,j3,j4,j5,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,1,2,2,1,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j5,j1,j2,j3,j4,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,2,2,1,2,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j4,j5,j1,j2,j3,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,2,1,2,1,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j3,j4,j5,j1,j2,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,1,2,1,2,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j2,j3,j4,j5,j1,za,zb,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o

      call helfill(2,1,2,1,1,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j1,j2,j3,j4,j5,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,2,1,1,2,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j5,j1,j2,j3,j4,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(2,1,1,2,1,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j4,j5,j1,j2,j3,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,1,2,1,2,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j3,j4,j5,j1,j2,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o
      call helfill(1,2,1,2,1,j1,j2,j3,j4,j5,h)
      call A51mpmpp5g(j2,j3,j4,j5,j1,zb,za,miA5t,A51h,A51o)
      miA5tree(h(1),h(2),h(3),h(4),h(5))=miA5t
      A51half(h(1),h(2),h(3),h(4),h(5))=A51h
      A51one(h(1),h(2),h(3),h(4),h(5))=A51o

      return
      end
