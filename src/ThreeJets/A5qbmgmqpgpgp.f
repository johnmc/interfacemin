      subroutine A5qbmgmqpgpgp(j1,j2,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
! Amplitudes for 0 -> qb- g- q+ g+ g+ from:
! 'One-Loop Corrections to Two-Quark Three-Gluon Amplitudes'
!  Zvi Bern, Lance Dixon, David A. Kosower
! Published in: Nucl.Phys.B 437 (1995) 259-304,
! e-Print: hep-ph/9409393 [hep-ph]
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'scale.f'
      include 'sprods_com.f'
      include 'zprods_decl.f'
      complex(dp) :: AL,ASUSY,Af,As
      complex(dp) :: miA5tree,VSUSY,VL,Vggluon,Vg13
      complex(dp) :: FSUSY,FS,FL,Ffgluon,Ff
      complex(dp) :: L0,L1,L2,Lsm1,Ls0,Ls1,Ls2,Ls3,lnrat
      integer j1,j2,j3,j4,j5

! Eq. (5.19)
      miA5tree=za(j1,j2)**3*za(j3,j2)/(za(j1,j2)*za(j2,j3)*za(j3,j4)*za(j4,j5)*za(j5,j1))
      
      Vggluon=-five*epinv*epinv2
     & -epinv*(lnrat(musq,-s(j1,j2))+lnrat(musq,-s(j2,j3))+lnrat(musq,-s(j3,j4))
     &        +lnrat(musq,-s(j4,j5))+lnrat(musq,-s(j5,j1)))
     & -half*(lnrat(musq,-s(j1,j2))**2+lnrat(musq,-s(j2,j3))**2+lnrat(musq,-s(j3,j4))**2
     &        +lnrat(musq,-s(j4,j5))**2+lnrat(musq,-s(j5,j1))**2)
     & +lnrat(-s(j1,j2),-s(j2,j3))*lnrat(-s(j3,j4),-s(j4,j5))
     & +lnrat(-s(j2,j3),-s(j3,j4))*lnrat(-s(j4,j5),-s(j5,j1))
     & +lnrat(-s(j3,j4),-s(j4,j5))*lnrat(-s(j5,j1),-s(j1,j2))
     & +lnrat(-s(j4,j5),-s(j5,j1))*lnrat(-s(j1,j2),-s(j2,j3))
     & +lnrat(-s(j5,j1),-s(j1,j2))*lnrat(-s(j2,j3),-s(j3,j4))
     & +five/six*pisq
      Vg13=Vggluon
     & +epinv*epinv2+epinv*lnrat(musq,-s(j1,j2))+half*lnrat(musq,-s(j1,j2))**2
     & +epinv*epinv2+epinv*lnrat(musq,-s(j2,j3))+half*lnrat(musq,-s(j2,j3))**2
      
      VSUSY=Vggluon-3._dp*epinv-1.5_dp*(lnrat(musq,-s(j2,j3))+lnrat(musq,-s(j5,j1)))-6._dp
      VL=Vg13-1.5_dp*(epinv+lnrat(musq,-s(j2,j3)))-3._dp
     & +Lsm1(-s(j3,j4),-s(j5,j1),-s(j2,j3),-s(j5,j1))
     & +Lsm1(-s(j1,j2),-s(j3,j4),-s(j5,j1),-s(j3,j4))
      
! Eq. (5.20)
      FSUSY =
     & 1.5_dp*za(j1,j2)*(za(j2,j3)*zb(j3,j4)*za(j4,j1)+za(j2,j4)*zb(j4,j5)*za(j5,j1))
     &  /(za(j3,j4)*za(j4,j5)*za(j5,j1))
     &  *L0(-s(j2,j3),-s(j5,j1))/s(j5,j1)
     
      Ffgluon = FSUSY*za(j1,j2)/(3*za(j3,j2))
      
      FS = 1._dp/3._dp*zb(j3,j4)*zb(j3,j5)/(zb(j1,j2)*zb(j2,j3)*za(j4,j5))
      
      FL = FS
     & +0.5_dp*za(j1,j5)*zb(j5,j4)**2*za(j4,j2)**2/(za(j3,j4)*za(j4,j5))
     &  *L1(-s(j2,j3),-s(j5,j1))/s(j5,j1)**2
     & +2*za(j1,j2)*za(j2,j4)*zb(j4,j5)*za(j5,j1)/(za(j3,j4)*za(j4,j5)*za(j5,j1))
     &  *L0(-s(j2,j3),-s(j5,j1))/s(j5,j1)
     & +0.5_dp*zb(j4,j5)*zb(j3,j5)*zb(j1,j3)/(zb(j2,j3)*zb(j1,j2)*zb(j1,j5)*za(j4,j5))

      Ff = czip

      AL=VL*miA5tree+FL
      ASUSY=VSUSY*miA5tree+FSUSY
! Vs = Vf =0, per Eq. (5.10)
      Af=Ff
      As=Fs

      return
      end
      
