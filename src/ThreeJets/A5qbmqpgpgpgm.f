      subroutine A5qbmqpgpgpgm(j1,j2,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
! Amplitudes for 0 -> qb- q+ g+ g+ g- from:
! 'One-Loop Corrections to Two-Quark Three-Gluon Amplitudes'
!  Zvi Bern, Lance Dixon, David A. Kosower
! Published in: Nucl.Phys.B 437 (1995) 259-304,
! e-Print: hep-ph/9409393 [hep-ph]
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'scale.f'
      include 'sprods_com.f'
      include 'zprods_decl.f'
      complex(dp) :: AL,ASUSY,Af,As
      complex(dp) :: miA5tree,VSUSY,VL,Vggluon,Vg12
      complex(dp) :: FSUSY,FS,FL,Ffgluon,Ff
      complex(dp) :: L0,L1,L2,Ls0,Ls1,Ls2,Ls3,lnrat
      integer j1,j2,j3,j4,j5

! Eq. (5.17)
      miA5tree=za(j1,j5)**3*za(j2,j5)/(za(j1,j2)*za(j2,j3)*za(j3,j4)*za(j4,j5)*za(j5,j1))
      
      Vggluon=-five*epinv*epinv2
     & -epinv*(lnrat(musq,-s(j1,j2))+lnrat(musq,-s(j2,j3))+lnrat(musq,-s(j3,j4))
     &        +lnrat(musq,-s(j4,j5))+lnrat(musq,-s(j5,j1)))
     & -half*(lnrat(musq,-s(j1,j2))**2+lnrat(musq,-s(j2,j3))**2+lnrat(musq,-s(j3,j4))**2
     &        +lnrat(musq,-s(j4,j5))**2+lnrat(musq,-s(j5,j1))**2)
     & +lnrat(-s(j1,j2),-s(j2,j3))*lnrat(-s(j3,j4),-s(j4,j5))
     & +lnrat(-s(j2,j3),-s(j3,j4))*lnrat(-s(j4,j5),-s(j5,j1))
     & +lnrat(-s(j3,j4),-s(j4,j5))*lnrat(-s(j5,j1),-s(j1,j2))
     & +lnrat(-s(j4,j5),-s(j5,j1))*lnrat(-s(j1,j2),-s(j2,j3))
     & +lnrat(-s(j5,j1),-s(j1,j2))*lnrat(-s(j2,j3),-s(j3,j4))
     & +five/six*pisq
      Vg12=Vggluon+epinv*epinv2+epinv*lnrat(musq,-s(j1,j2))+half*lnrat(musq,-s(j1,j2))**2
      
      VSUSY=Vggluon-3._dp*epinv-1.5_dp*(lnrat(musq,-s(j1,j2))+lnrat(musq,-s(j4,j5)))-6._dp
      VL=Vg12-1.5_dp*(epinv+lnrat(musq,-s(j4,j5)))-5._dp/2._dp
      
! Eq. (5.18)
      FSUSY =
     & 1.5_dp*za(j1,j5)*za(j2,j5)/(za(j1,j2)*za(j2,j3)*za(j3,j4)*za(j4,j5))
     & *(za(j5,j4)*zb(j4,j3)*za(j3,j1)+za(j5,j3)*zb(j3,j2)*za(j2,j1))
     & *L0(-s(j1,j2),-s(j4,j5))/s(j4,j5)
     
      Ffgluon = FSUSY*za(j5,j1)/(3*za(j5,j2))
      
      FS = 1._dp/3._dp*(
     & -za(j1,j3)**2*za(j4,j5)**2*zb(j3,j4)**3/(za(j1,j2)*za(j3,j4))
     &  *2*L2(-s(j4,j5),-s(j1,j2))/s(j1,j2)**3
     & +za(j1,j3)*za(j4,j5)*za(j1,j5)*zb(j3,j4)**2/(za(j1,j2)*za(j3,j4))
     &  *3*L1(-s(j4,j5),-s(j1,j2))/s(j1,j2)**2
     & +2*za(j1,j5)**2*zb(j3,j4)/(za(j1,j2)**2*zb(j1,j2)*za(j3,j4))
     & +za(j1,j5)*zb(j1,j3)*zb(j2,j4)/(za(j1,j2)*zb(j1,j2)*za(j3,j4)*zb(j1,j5))
     & +za(j1,j4)*zb(j1,j4)*zb(j2,j4)*zb(j3,j4)
     &  /(za(j1,j2)*zb(j1,j2)*za(j3,j4)*zb(j4,j5)*zb(j1,j5)))
      
      FL = FS
     & -za(j1,j5)**2*za(j1,j4)*zb(j1,j4)**2/(za(j2,j3)*za(j3,j4))
     &  *Ls1(-s(j4,j5),-s(j2,j3),-s(j5,j1),-s(j2,j3))/s(j2,j3)**2
     & -za(j1,j5)**2*za(j3,j5)*za(j1,j3)*zb(j1,j3)**2/(za(j2,j3)*za(j3,j4)*za(j4,j5))
     &  *Ls1(-s(j1,j2),-s(j4,j5),-s(j2,j3),-s(j4,j5))/s(j4,j5)**2
     & -za(j1,j5)**2*zb(j1,j4)/(za(j2,j3)*za(j3,j4))
     &  *L0(-s(j2,j3),-s(j5,j1))/s(j5,j1)
     & +za(j1,j5)**2*zb(j1,j2)*za(j2,j5)/(za(j2,j3)*za(j3,j4)*za(j4,j5))
     &  *L0(-s(j2,j3),-s(j4,j5))/s(j4,j5)
     & -(za(j1,j5)**2*zb(j3,j4)/(za(j1,j2)*za(j3,j4))
     &  -0.5_dp*za(j1,j5)*za(j2,j5)
     &     *(3*za(j5,j4)*zb(j4,j3)*za(j3,j1)+za(j5,j3)*zb(j3,j2)*za(j2,j1))
     &     /(za(j1,j2)*za(j2,j3)*za(j3,j4)*za(j4,j5)))
     &  *L0(-s(j1,j2),-s(j4,j5))/s(j4,j5)
     & +0.5_dp*(-za(j1,j3)*za(j3,j5)*za(j4,j5)*zb(j3,j4)**2/(za(j2,j3)*za(j3,j4))
     &  *L1(-s(j1,j2),-s(j4,j5))/s(j4,j5)**2
     &  +zb(j2,j4)*zb(j3,j4)/(za(j3,j4)*zb(j4,j5)*zb(j1,j5)))

      Ff = -za(j1,j5)**2*zb(j3,j4)/(za(j1,j2)*za(j3,j4))
     & *L0(-s(j1,j2),-s(j4,j5))/s(j4,j5)
     
      AL=VL*miA5tree+FL
      ASUSY=VSUSY*miA5tree+FSUSY
! Vs = Vf =0, per Eq. (5.10)
      Af=Ff
      As=Fs

      return
      end
      
