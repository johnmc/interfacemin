      function A5NLOgggggold()
! Implementation of hep-ph/9302280, Eq. (11)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'qcdcouple.f'
      include 'zprods_com.f'
! list of permuation of S5/Z5
      integer, parameter, dimension(5,24)  :: S5Z5(5,24) = reshape([
     & 1, 2, 3, 4, 5,    1, 2, 4, 3, 5,    1, 3, 2, 4, 5,    1, 3, 4, 2, 5,
     & 1, 4, 2, 3, 5,    1, 4, 3, 2, 5,    2, 1, 3, 4, 5,    2, 1, 4, 3, 5,
     & 2, 3, 1, 4, 5,    2, 3, 4, 1, 5,    2, 4, 1, 3, 5,    2, 4, 3, 1, 5,
     & 3, 1, 2, 4, 5,    3, 1, 4, 2, 5,    3, 2, 1, 4, 5,    3, 2, 4, 1, 5,
     & 3, 4, 1, 2, 5,    3, 4, 2, 1, 5,    4, 1, 2, 3, 5,    4, 1, 3, 2, 5,
     & 4, 2, 1, 3, 5,    4, 2, 3, 1, 5,    4, 3, 1, 2, 5,    4, 3, 2, 1, 5 ], [5,24])
      integer ro,pp,ih,h1,h2,h3,h4,h5,getS5Z5,rDro,i1,i2,i3,i4,i5
! list of permutations in H5
!      integer, parameter, dimension(5,24)  :: H5(5,6) = reshape([
!     & 1, 2, 3, 4, 5,    3, 4, 1, 2, 5,    3, 1, 2, 4, 5,    2, 1, 3, 4, 5,
!     & 2, 1, 3, 4, 5,    3, 2, 1, 4, 5,    3, 4, 2, 1, 5 ], [5,6])
! pointer to action of r=(2,4,1,3,5) on S5Z5, e.g. element 1 = (1,2,3,4,5) -> (2,4,1,3,5) = element 11
      integer, parameter, dimension(24) :: rdotrho = [ 
     & 11,  9, 17, 15, 23, 21,  5,  3, 18, 13, 24, 19,  6,  1, 12,  7, 22, 20,  4,  2, 10,  8, 16, 14 ]
! pointer to P(5 3)
      integer, parameter, dimension(10) :: P53 = [ 1, 2, 17, 4, 11, 9, 10, 5 ,3 ,1 ]
! pointer to action of h (in H5) on P53
      integer, parameter, dimension(10,6)  :: hdotp = reshape([
     & 1, 2,17, 4,11, 9,10, 5, 3, 1,17,23, 1,21, 3, 5,19, 9,11,17,13,19, 4,20, 2, 1,22, 8, 7,13,
     & 7, 8,23,14,21,15,16,19,13, 7,15,21, 6,23, 5, 3,24,11, 9,15,18,24, 2,22, 4, 6,20,10,12,18], [10,6])
      complex(dp) :: A50(2,2,2,2,2),A51one(2,2,2,2,2),A51half(2,2,2,2,2),
     & A5tree(5,5,5,5,5,2,2,2,2,2),A51(5,5,5,5,5,2,2,2,2,2),A53(5,5,5,5,5,2,2,2,2,2),A53bit,A51gluon(5,5,5,5,5,2,2,2,2,2)
      real(dp) :: A5NLOgggggold
      complex(dp) :: sum,A5treec
      real(dp) :: pin(mxpart,4),p(mxpart,4)

! statement function 
      A5treec(i1,i2,i3,i4,i5,h1,h2,h3,h4,h5)=Conjg(A5tree(i1,i2,i3,i4,i5,h1,h2,h3,h4,h5))
 
! Set up basic amplitudes
      do ro=1,24
        call A5gfill(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),za,zb,A50,A51half,A51one)
        A5tree(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),:,:,:,:,:)=A50(:,:,:,:,:)
        A51gluon(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),:,:,:,:,:)=A51one(:,:,:,:,:)
        A51(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),:,:,:,:,:)=A51one(:,:,:,:,:)+float(nflav)/xn*A51half(:,:,:,:,:)
      enddo
        
! Performu sum according to Eq. (11)
      A5NLOgggggold=0d0
      do h1=1,2
      do h2=1,2
      do h3=1,2
      do h4=1,2
      do h5=1,2
      do ro=1,24

        A5NLOgggggold=A5NLOgggggold
     &   +real(conjg(A5tree(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),h1,h2,h3,h4,h5))
     &                 *A51(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),h1,h2,h3,h4,h5),kind=dp)

     &   +real(conjg(A5tree(S5Z5(2,ro), S5Z5(4,ro), S5Z5(1,ro), S5Z5(3,ro), S5Z5(5,ro),h1,h2,h3,h4,h5))
     &                 *A51(S5Z5(1,ro), S5Z5(2,ro), S5Z5(3,ro), S5Z5(4,ro), S5Z5(5,ro),h1,h2,h3,h4,h5),kind=dp)*two/xn**2


      enddo

c--- A53 bit
      A5NLOgggggold=A5NLOgggggold + real(5*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3
     &,4,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 4*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,3,1,4,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 5*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,4,2,1,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,2,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,3,2,4,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,3,1,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(1,2,4,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(4,3,1,2,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,1,4,3,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,2,4,1,5,h1,h2,h3,h4,h5)
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,3,2,4,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,4,2,3,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,1,4,2,5,h1,h2,h3,h4,h5)
     &  + A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(1,3,4,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(4,3,1,2,5,h1,h2,h3,h4,h5)
     &  + 4*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,1,4,3,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,4,1,2,5,h1,h2,h3,h4,h5)
     &  + A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,3,2,4,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,1,4,2,5,h1,h2,h3,h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(1,4,3,2,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 3*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,3,2,4,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,1,2,4,5,h1,h2,h3,h4,h5)
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(2,1,3,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(4,2,1,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,1,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,1,4,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,3,4,2,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,3,1,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(4,2,3,1,5,h1,h2,h3,h4,h5)
     &  + A5treec(2,3,1,4,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,1,4,3,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,4,2,1,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,3,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + 5*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,3,4,2,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 5*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,3,1,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(4,2,1,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,3,1,4,5,h1,h2,h3,h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(2,4,3,1,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 4*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 3*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,2,4,3,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,1,3,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(4,1,2,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,1,2,4,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,4,2,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,2,1,4,5,h1,h2,h3,h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,1,4,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,1,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,2,4,1,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 4*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,2,3,4,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 3*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,3,4,1,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 4*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + 2*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,4,2,1,5,h1,h2,h3,h4,h5)
     &  + 3*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,4,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,4,2,3,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(3,2,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,4,1,2,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(3,4,2,1,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 3*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,4,2,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,1,4,2,5,h1,h2,h3,h4,h5)
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 3*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,1,2,3,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,
     & h3,h4,h5)
     &
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,2,3,4,5,h1,h2,h3,h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(3,2,4,1,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,4,2,1,5,h1,h2,h3,h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,1,3,2,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,4,2,3,5,h1,h2,h3,h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,1,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(2,4,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,1,3,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,2,3,4,5,h1,h2,h3,h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,1,3,4,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(2,4,3,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*
     & A51gluon(3,1,2,4,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(3,4,2,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(4,2,1,3,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,2,3,1,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,h3,
     & h4,h5)
     &
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(1,2,3,4,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,2,4,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,2,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,3,4,2,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(1,4,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,1,4,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(2,3,4,1,5,h1,h2,h3,
     & h4,h5)
     &
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*
     & A51gluon(2,4,3,1,5,h1,h2,h3,h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,1,2,4,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,1,4,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(3,4,1,2,5,h1,h2,
     & h3,h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,2,3,5,h1,h2,
     & h3,h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,1,3,2,5,h1,h2,h3,
     & h4,h5)
     &  + A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,2,3,1,5,h1,h2,h3,
     & h4,h5)
     &  + 2*A5treec(4,3,1,2,5,h1,h2,h3,h4,h5)*A51gluon(4,3,1,2,5,h1,h2,
     & h3,h4,h5)
     & ,kind=dp)*two/xn**2
      enddo
      enddo
      enddo
      enddo
      enddo


! Remember that we have removed a factor of 16*pi^2 from the amplitudes
      A5NLOgggggold=two*gsq**3*ason4pi*xn**4*V*A5NLOgggggold
      
      return
      end
      
      










