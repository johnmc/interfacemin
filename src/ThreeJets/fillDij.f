      subroutine fillDij(za,zb,s,aQQ_qb0,DQQ_qb,DQQ_QQb)
!--- finite contributions from 9405386, Eqs. (17)-(23)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'scale.f'
      include 'sprods_decl.f'
      include 'zprods_decl.f'
      integer :: q, QQ, qb, QQb, g
      integer :: P, Pb
      integer i,j,k,l,ipass
      complex(dp) :: lnrat,bra0,bra1,bra2,bra3
      complex(dp) :: aQQ_qb0(2,2),DQQ_QQB(2,2),DQQ_qb(2,2)

! curly-braces notation in Eq. (18)
      bra0(i,j,k,l)=lnrat(-s(i,j),musq)-lnrat(-s(k,l),musq)
      bra1(i,j,k,l)=bra0(i,j,k,l)/(s(k,l)-s(i,j))
      bra2(i,j,k,l)=bra0(i,j,k,l)/(s(k,l)-s(i,j))**2
     &  +one/(s(k,l)*(s(k,l)-s(i,j)))
      bra3(i,j,k,l)=bra0(i,j,k,l)/(s(k,l)-s(i,j))**3
     &  +(s(i,j)+s(k,l))/(2*s(i,j)*s(k,l)*(s(k,l)-s(i,j))**2)


      do ipass=2,1,-1
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
! and that the ipass=2 corresponds to qb<->q, Qbar<->Q, c.f. Eqs. (14) and (15)
      if (ipass == 1) then
         q=1
         QQ=2
         qb=3
         QQb=4
         g=5
      else
         q=3
         QQ=4
         qb=1
         QQb=2
         g=5
      endif
      P=QQ
      Pb=QQb
! Eq. (19)
      DQQ_QQB(2,2)=
     & xn*(za(qb,P)**2*za(Pb,g)*zb(P,g)/(za(qb,q)*za(P,g)**2)
     &    +3*za(qb,Pb)*za(qb,P)*zb(P,g)/(2*za(qb,q)*za(P,g)))
     &  *bra1(Pb,g,qb,q)
     & -(xn**2+one)/xn*(
     &  za(qb,Pb)*za(Pb,P)*zb(q,P)/(2*za(Pb,g)*za(P,g)*s(qb,q))
     &  +za(qb,Pb)*zb(q,g)/(za(P,g)*s(qb,q))
     &  +za(qb,P)*za(Pb,P)*zb(q,P)*zb(P,g)/(2*za(P,g))*bra2(Pb,g,qb,q)
     &  -za(qb,Pb)*za(qb,P)/(za(qb,q)*za(P,g)**2)*bra0(Pb,P,qb,q)
     &  +za(qb,P)*za(Pb,g)*zb(q,g)/za(P,g)**2*bra1(Pb,P,qb,q)
     &  -za(qb,g)*za(Pb,P)*zb(q,g)*zb(P,g)/za(P,g)*bra2(Pb,P,qb,q))
     & -one/xn*(
     &  za(qb,Pb)/(za(q,g)*za(P,g))*bra0(Pb,g,qb,P)
     &  +za(qb,Pb)*za(qb,P)/(za(qb,q)*za(P,g)**2)*bra0(Pb,g,qb,q)
     &  +3*za(qb,Pb)**2/(2*za(qb,q)*za(Pb,g)*za(P,g))*bra0(Pb,P,Pb,g)
     &  -(za(qb,P)*za(q,g)*za(Pb,P)**2*zb(q,P)/(za(q,P)*za(Pb,g)*za(P,g)**2)
     &   -za(qb,Pb)*za(Pb,P)*zb(q,P)/(2*za(Pb,g)*za(P,g)))*bra1(Pb,g,qb,q)
     &  +za(qb,g)*za(q,Pb)*zb(q,g)/(za(q,g)*za(P,g))*bra1(q,Pb,qb,P)
     &  -za(qb,P)*za(q,Pb)**2*zb(q,P)/(za(q,P)*za(q,g)*za(Pb,g))
     &   *bra1(Pb,g,qb,P))

! Eq. (20)
      DQQ_QQB(1,2)=
     & xn*(3*za(q,Pb)**2/(2*za(qb,q)*za(Pb,g)*za(P,g))*bra0(Pb,g,qb,q)
     &   +3*za(q,Pb)*za(Pb,P)*zb(qb,P)/(za(Pb,g)*za(P,g))*bra1(Pb,g,qb,q))
     & +one/xn*3*za(q,Pb)**2/(2*za(qb,q)*za(Pb,g)*za(P,g))*bra0(Pb,P,Pb,g)
     & -(xn**2+one)/xn*(
     &  za(q,Pb)*zb(qb,g)/(za(P,g)*s(qb,q))
     &  +za(q,Pb)/(za(qb,g)*za(P,g))*bra0(q,P,Pb,g)
     &  +za(q,Pb)*za(q,P)/(za(qb,q)*za(P,g)**2)*bra0(Pb,P,Pb,g)
     &  -za(qb,Pb)*za(q,g)*zb(qb,g)/(za(qb,g)*za(P,g))*bra1(q,P,qb,Pb)
     &  +za(q,P)*za(Pb,g)*zb(qb,g)/za(P,g)**2*bra1(Pb,P,qb,q)
     &  -2*za(q,g)*za(Pb,P)**2*zb(qb,P)/(za(Pb,g)*za(P,g)**2)*bra1(Pb,g,qb,q)
     &  +za(qb,g)*za(q,P)*za(Pb,P)**2*zb(qb,P)/(za(qb,P)*za(Pb,g)*za(P,g)**2)
     &   *bra1(Pb,g,qb,q)
     &  +za(qb,Pb)**2*za(q,P)*zb(qb,P)/(za(qb,P)*za(qb,g)*za(Pb,g))*bra1(Pb,g,q,P)
     &  -za(q,g)*za(Pb,P)*zb(qb,g)*zb(P,g)/za(P,g)*bra2(Pb,P,qb,q)
     & -za(qb,q)*za(Pb,P)**2*zb(qb,P)**2/(2*za(Pb,g)*za(P,g))*bra2(Pb,g,qb,q))
     
! Eqs. (14) and (16)
      if (ipass == 2) then
        DQQ_QQB(1,1)=-DQQ_QQB(2,2)
        DQQ_QQB(2,1)=-DQQ_QQB(1,2)
      endif
      enddo
     
      do ipass=2,1,-1
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
! and that ipass=2 corresponds to qb<->Q, q<->Qbar, c.f. Eq. (17)
      if (ipass == 1) then
         q=1
         QQ=2
         qb=3
         QQb=4
         g=5
      else
         q=4
         QQ=3
         qb=2
         QQb=1
         g=5
      endif
      P=QQ
      Pb=QQb
! Eq. (21)
      DQQ_qb(2,2)=
     & 2*(xn-float(nflav))/three*(
     &  za(qb,Pb)**2*zb(Pb,g)/(za(qb,q)*za(P,g))*bra1(Pb,P,qb,q)
     &  -za(qb,Pb)*zb(q,g)*zb(P,g)/two*bra2(Pb,P,qb,q)
     &  +za(qb,g)*za(Pb,P)*zb(q,g)*zb(P,g)**2*bra3(Pb,P,qb,q))
     & +(xn**2+one)/xn*(
     &  -za(qb,Pb)**2*za(q,P)/(2*za(qb,q)*za(q,g)*za(Pb,P)*za(P,g))
     &  -za(qb,Pb)*zb(q,g)/(two*za(P,g)*s(qb,q))
     &  -za(qb,q)*za(Pb,g)*za(qb,Pb)/(za(q,g)**2*za(Pb,P)*za(qb,g))*bra0(Pb,P,qb,g)
     &  +za(qb,q)**2*zb(q,P)*(za(Pb,q)*za(P,g)+za(Pb,P)*za(q,g))
     &   /(za(qb,g)*za(q,P)*za(q,g)**2)*bra1(Pb,P,qb,g)
     &  -2*za(qb,P)**2*za(q,Pb)*zb(q,P)/(za(qb,g)*za(q,P)*za(P,g))*bra1(q,Pb,qb,g)
     &  +(-3*za(qb,Pb)*zb(q,g)/(2*za(P,g))+za(qb,g)*za(q,Pb)*zb(P,g)/za(q,g)**2
     &   -2*za(qb,P)*za(Pb,g)*zb(P,g)/(za(q,g)*za(P,g)))*bra1(Pb,P,qb,q)
     &  +za(Pb,P)/two*(za(qb,g)*zb(q,g)*zb(P,g)/za(P,g)*bra2(Pb,P,qb,q)
     &   -za(qb,g)*zb(P,g)**2/za(q,g)*bra2(qb,q,Pb,P)
     &   -za(qb,q)**2*zb(q,P)**2/(za(qb,g)*za(q,g))*bra2(qb,g,Pb,P)))
     & +xn*(
     &  za(qb,Pb)*za(qb,P)*za(Pb,g)/(za(qb,g)*za(q,g)*za(Pb,P)*za(P,g))*bra0(Pb,P,q,Pb)
     &  +(za(qb,g)*za(q,Pb)/(za(q,g)*za(qb,Pb))+half)
     &   *za(qb,Pb)**2*za(q,P)/(za(qb,q)*za(q,g)*za(Pb,P)*za(P,g))*bra0(Pb,P,qb,q)
     &  +3*za(qb,Pb)**2/(2*za(qb,g)*za(q,g)*za(Pb,P))*bra0(Pb,P,qb,g)
     &  +(za(q,Pb)*za(P,g)/(za(q,g)*za(Pb,P))-two)
     &   *za(qb,Pb)*za(qb,P)/(za(qb,g)*za(q,P)*za(P,g))*bra0(q,Pb,qb,g)
     &  -3*za(qb,P)*(za(Pb,P)*zb(P,g)/(za(q,P)*za(P,g))*bra1(q,Pb,qb,g)
     &  +za(qb,g)*za(q,Pb)*zb(P,g)/(za(qb,q)*za(q,g)*za(P,g))*bra1(Pb,P,qb,q)
     &  -za(qb,q)*za(q,Pb)*zb(q,P)/(za(qb,g)*za(q,P)*za(q,g))*bra1(Pb,P,qb,g)))
     & +one/xn*(
     &  za(qb,q)*za(qb,Pb)*za(Pb,g)/(za(qb,g)*za(q,g)**2*za(Pb,P))*bra0(Pb,P,qb,q)
     &  +za(qb,Pb)**2/(2*za(qb,g)*za(q,g)*za(Pb,P))*bra0(qb,g,qb,q)
     &  +2*za(qb,Pb)/(za(q,g)*za(P,g))*bra0(qb,g,qb,q))

! Eq. (17), noting that lnrat term needs to have qb <-> Q, q <-> Qb applied
      if (ipass == 2) then
        DQQ_qB(1,1)=-DQQ_qB(2,2)
     &    +two/three*(xn-float(nflav))*aQQ_qb0(1,1)*lnrat(-s(q,qb),-s(P,Pb))
      endif
      enddo

      q=1
      QQ=2
      qb=3
      QQb=4
      g=5
      P=QQ
      Pb=QQb

! Eq. (22)
      DQQ_qb(1,2)=
     & (xn**2+one)/xn*(
     &  za(qb,P)*za(q,Pb)**2/(2*za(qb,q)*za(qb,g)*za(Pb,P)*za(P,g))
     &  -za(q,Pb)*zb(qb,g)/(2*za(P,g)*s(qb,q))
     &  +za(q,g)*zb(P,g)**2/(2*za(qb,g)*zb(Pb,P)*s(qb,q))
     &  -za(q,Pb)*zb(P,g)/za(qb,g)*bra1(Pb,P,qb,q)
     &  +za(q,g)*za(Pb,P)*zb(P,g)*(s(P,g)-s(qb,g))/(2*za(qb,g)*za(P,g))*bra2(Pb,P,qb,q))
     & +float(nflav)/three*(
     &  -2*za(q,Pb)**2*za(q,P)/(za(qb,q)*za(q,g)*za(Pb,P)*za(P,g))*bra0(Pb,P,qb,q)
     &  -2*za(q,Pb)**2*zb(qb,g)/(za(q,g)*za(Pb,P))*bra1(Pb,P,qb,q)
     &  +za(q,Pb)*zb(qb,g)*zb(P,g)*bra2(qb,q,Pb,P)
     &  -2*za(qb,q)*za(Pb,g)*zb(qb,g)**2*zb(P,g)*bra3(Pb,P,qb,q))
     & -one/xn*(
     &  2*za(q,Pb)/(za(qb,g)*za(P,g))*bra0(qb,Pb,Pb,P)
     &  -2*za(q,P)*za(Pb,g)*zb(P,g)/(za(qb,g)*za(P,g))*bra1(q,P,qb,Pb)
     &  -(3*za(Pb,q)/(2*za(Pb,g))+2*za(qb,q)/za(qb,g))
     &   *za(Pb,g)*zb(qb,g)/za(P,g)*bra1(Pb,P,qb,q))
     & +xn*(
     &  -3*za(q,Pb)**2*za(qb,Pb)/(2*za(qb,q)*za(Pb,P)*za(qb,g)*za(Pb,g))*bra0(Pb,P,qb,q)
     & +(3*za(q,Pb)*za(q,g)*zb(P,g)*(za(qb,P)*za(Pb,g)+za(qb,Pb)*za(P,g))
     &   /(2*za(qb,q)*za(qb,g)*za(Pb,g)*za(P,g))
     &  -2*za(q,Pb)**2*zb(Pb,g)/(3*za(qb,q)*za(P,g)))*bra1(Pb,P,qb,q)
     &  -za(q,Pb)*zb(qb,g)*zb(P,g)/three*bra2(Pb,P,qb,q)
     &  +2*za(q,g)*za(Pb,P)*zb(qb,g)*zb(P,g)**2/three*bra3(Pb,P,qb,q))

! Eq. (23)
      DQQ_qb(2,1)=
     & 2*(float(nflav)-xn)/three*(
     &  za(qb,P)**2*zb(Pb,g)/(za(qb,q)*za(P,g))*bra1(Pb,P,qb,q)
     &  +za(qb,P)*zb(q,g)*zb(Pb,g)/two*bra2(Pb,P,qb,q)
     &  +za(qb,g)*za(Pb,P)*zb(q,g)*zb(Pb,g)**2*bra3(Pb,P,qb,q))
     & +xn*(
     &  za(qb,P)**3/(2*za(qb,q)*za(qb,g)*za(Pb,P)*za(P,g))
     &  -(za(qb,g)*za(q,P)**2*zb(q,g)/(za(q,g)**2*za(Pb,P))
     &  +3*za(qb,P)*za(q,P)*zb(q,g)/(2*za(q,g)*za(Pb,P)))*bra1(Pb,P,qb,g)
     &  -(za(qb,g)*za(q,P)**2*zb(q,g)/(za(q,g)**2*za(Pb,P))
     &   +za(qb,P)*za(q,Pb)*za(P,g)*zb(q,g)/(2*za(q,g)*za(Pb,P)*za(Pb,g))
     &   -za(qb,P)**2*zb(P,g)/(2*za(qb,q)*za(Pb,g))
     &   +za(qb,Pb)**2*za(P,g)*zb(Pb,g)/(za(qb,q)*za(Pb,g)**2))*bra1(Pb,P,qb,q)
     &  -(3*za(qb,Pb)*za(qb,P)*zb(Pb,g)/(2*za(qb,q)*za(Pb,g))
     &   +za(qb,Pb)**2*za(P,g)*zb(Pb,g)/(za(qb,q)*za(Pb,g)**2))*bra1(P,g,qb,q)
     &  -za(qb,Pb)*za(Pb,P)*zb(q,Pb)*zb(Pb,g)/(2*za(Pb,g))*bra2(P,g,qb,q)
     &  +za(qb,g)*za(Pb,P)*zb(q,g)*zb(Pb,g)/(2*za(Pb,g))*bra2(Pb,P,qb,q)
     &  +za(qb,q)*za(P,g)*zb(q,g)*zb(Pb,g)/(2*za(q,g))*bra2(qb,q,Pb,P)
     &  -za(qb,q)*za(q,P)*zb(q,Pb)*zb(q,g)/(2*za(q,g))*bra2(qb,g,Pb,P))
     & +one/xn*(
     &  za(qb,P)**2*za(q,P)/(2*za(qb,q)*za(q,g)*za(Pb,P)*za(P,g))
     &  +za(qb,P)*za(Pb,P)*zb(q,Pb)/(2*za(Pb,g)*za(P,g)*s(qb,q))
     &  +za(qb,g)*za(Pb,P)*zb(Pb,g)/(2*za(q,g)*za(Pb,g)*s(qb,q))
     &  +za(qb,g)*zb(Pb,g)**2/(2*za(q,g)*zb(Pb,P)*s(qb,q))
     &  -2*za(qb,P)*za(q,P)/(za(q,Pb)*za(q,g)*za(P,g))*bra0(qb,Pb,qb,q)
     &  -za(qb,P)**2/(2*za(qb,g)*za(q,g)*za(Pb,P))*bra0(qb,g,qb,q)
     &  -2*za(qb,q)*za(qb,P)/(za(qb,g)*za(q,Pb)*za(q,g))*bra0(q,P,qb,g)
     &  +(za(qb,g)*za(Pb,P)*za(qb,P)/(za(Pb,g)**2*za(P,g)*za(qb,q))
     &   -za(qb,P)*za(qb,q)*za(P,g)/(za(qb,g)*za(q,g)**2*za(Pb,P)))*bra0(Pb,P,qb,q)
     &  +za(qb,q)*za(P,g)*zb(Pb,g)/(za(q,g)**2*s(Pb,P))*bra0(Pb,P,qb,g)
     &  +(2*za(qb,P)*za(Pb,P)/(za(q,Pb)*za(Pb,g)*za(P,g))
     &   -za(qb,g)*za(Pb,P)*zb(q,g)/(za(Pb,g)**2*s(qb,q))
     &   -za(qb,P)**2/(2*za(qb,q)*za(Pb,g)*za(P,g)))*bra0(P,g,qb,q)
     &  -(za(qb,g)*za(Pb,P)*zb(q,g)/za(Pb,g)**2
     &   +2*(za(qb,P)*za(Pb,g)+za(qb,g)*za(Pb,P))*zb(Pb,g)/(za(q,g)*za(Pb,g))
     &   +za(qb,g)**2*za(q,P)*za(Pb,P)*zb(Pb,g)/(2*za(qb,q)*za(q,g)*za(Pb,g)*za(P,g))
     &   -za(qb,g)*za(q,P)*zb(Pb,g)/za(q,g)**2)*bra1(Pb,P,qb,q)
     &  -2*za(qb,Pb)*za(P,g)*zb(Pb,g)/(za(q,g)*za(Pb,g))*bra1(q,P,qb,Pb)
     &  +2*za(qb,Pb)*za(Pb,P)*zb(Pb,g)/(za(q,Pb)*za(Pb,g))*bra1(q,P,qb,g)
     &  +(2*za(qb,q)**2*za(Pb,P)*zb(q,Pb)/(za(qb,g)*za(q,Pb)*za(q,g))
     &   +za(qb,q)**2*za(P,g)*zb(qb,g)*zb(q,Pb)/(za(q,g)**2*s(Pb,P)))*bra1(Pb,P,qb,g)
     &  +2*za(qb,q)*za(q,P)*zb(q,g)/(za(q,Pb)*za(q,g))*bra1(P,g,qb,Pb)
     &  +(2*za(qb,q)*za(Pb,P)**2*zb(q,Pb)/(za(q,Pb)*za(Pb,g)*za(P,g))
     &   +za(qb,g)*za(Pb,P)**2*zb(q,Pb)*zb(P,g)/(za(Pb,g)**2*s(qb,q)))*bra1(P,g,qb,q)
     &  +za(qb,q)**2*za(Pb,P)*zb(q,Pb)**2/(2*za(qb,g)*za(q,g))*bra2(qb,g,Pb,P)
     &  +za(qb,g)*za(Pb,P)*zb(Pb,g)*(s(q,P)-s(qb,Pb))/(2*za(q,g)*za(Pb,g))*bra2(Pb,P,qb,q)
     &  +za(qb,q)*za(Pb,P)**2*zb(q,Pb)**2/(2*za(Pb,g)*za(P,g))*bra2(P,g,qb,q))

      return
      end
