      subroutine fillEij(za,zb,s,aQQ_qb0,aQQ_QQb0,aq_qb0,aq_QQb0,EQQ_qb,EQQ_QQb)
!--- pentagon and box finite contributions from 9405386, Eqs. (11)-(13)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'scale.f'
      include 'sprods_decl.f'
      include 'zprods_decl.f'
      integer :: q, QQ, qb, QQb, g
      integer :: P, Pb
! definitions of helicity-dependent momenta , Eq. (5)
      integer :: pntr_r(2), pntr_RR(2)
      complex(dp) :: aQQ_qb0(2,2),aQQ_QQb0(2,2),aq_qb0(2,2),aq_QQb0(2,2)
      complex(dp) :: EQQ_QQb(2,2),EQQ_qb(2,2)
      complex(dp) :: h,htil,FF,GGg4,GGg2,HQQ_QQB(2),HQQ_qb(2),Hq_QQB(2)
      complex(dp) :: aQQ_QQb0_plus(2),aQQ_qb0_plus(2),aq_QQb0_plus(2)
      integer hq,hQQ,i,j,k,l,krondel,r,a,gg,AA,RR

! statement functions
! Eq. (26)
      h(i,j,k,l)=(za(i,k)*za(j,l)/(za(i,l)*za(j,k)))**2
! Eq. (27)
      htil(i,j,k,l)=h(i,j,k,l)**((1-krondel(i,k))*(1-krondel(j,l)))
! Eq. (28)
      GGg4(r,a,AA,gg,RR)=h(r,a,gg,RR)**krondel(RR,AA)*FF(a,AA,gg,s)
      GGg2(r,gg,a,AA,RR)=h(r,gg,AA,RR)**krondel(r,a)*FF(gg,a,AA,s)
      
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
      q=1
      QQ=2
      qb=3
      QQb=4
      g=5
      P=QQ
      Pb=QQb
      pntr_r = [q, qb]
      pntr_RR = [QQ, QQb]
      
      do hq=1,2
      r=pntr_r(hq)
      do hQQ=1,2
      RR=pntr_RR(hQQ)

! Eq. (29)
      EQQ_qb(hq,hQQ)=
     & -aQQ_qb0(hq,hQQ)*(xn*(
     &   FF(qb,g,P,s)+h(r,qb,Pb,RR)*FF(qb,q,Pb,s)
     &   +h(r,q,g,RR)*FF(q,qb,g,s)+h(r,g,Pb,RR)*FF(g,P,Pb,s)
     &   +h(r,q,P,RR)*FF(q,Pb,P,s))
     &  -one/xn*(
     &   -h(r,q,Pb,RR)*(FF(q,P,Pb,s)+FF(q,qb,Pb,s))+FF(q,g,Pb,s)
     &   -h(r,qb,P,RR)*(FF(qb,Pb,P,s)+FF(qb,q,P,s))+FF(qb,g,P,s)
     &   +h(r,qb,Pb,RR)*(FF(qb,P,Pb,s)+FF(qb,q,Pb,s))-FF(qb,g,Pb,s)
     &   +h(r,q,P,RR)*(FF(q,qb,P,s)+FF(q,Pb,P,s))-FF(q,g,P,s))
     &   )
     & +aQQ_QQb0(hq,hQQ)*(xn*h(r,g,Pb,RR)*FF(g,P,Pb,s)
     &  -one/xn*(
     &   -FF(qb,g,Pb,s)+h(r,qb,g,RR)*FF(qb,q,g,s)
     &   +FF(Pb,g,q,s)-h(r,q,g,RR)*FF(q,qb,g,s)
     &   -htil(r,Pb,g,RR)*FF(Pb,P,g,s)
     &   -h(r,q,Pb,RR)*(FF(q,P,Pb,s)-FF(q,qb,Pb,s))
     &   +h(r,qb,Pb,RR)*(FF(qb,P,Pb,s)-FF(qb,q,Pb,s))
     &   +GGg4(r,qb,Pb,g,RR)-GGg4(r,qb,P,g,RR)
     &   +GGg4(r,q,P,g,RR)-GGg4(r,q,Pb,g,RR))
     &   )
     & +aq_qb0(hq,hQQ)*(xn*h(r,q,g,RR)*FF(q,qb,g,s)
     &  -one/xn*(
     &   FF(Pb,g,q,s)-h(r,g,Pb,RR)*FF(g,P,Pb,s)
     &   -FF(P,g,q,s)+h(r,g,P,RR)*FF(g,Pb,P,s)
     &   -htil(r,g,q,RR)*FF(g,qb,q,s)
     &   -h(r,q,Pb,RR)*(FF(q,qb,Pb,s)-FF(q,P,Pb,s))
     &   +h(r,q,P,RR)*(FF(q,qb,P,s)-FF(q,Pb,P,s))
     &   +GGg2(r,g,qb,Pb,RR)-GGg2(r,g,q,Pb,RR)
     &   +GGg2(r,g,q,P,RR)-GGg2(r,g,qb,P,RR))
     &  )
     
      enddo
      enddo


      do hq=1,2
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
! and that the hq=1 corresponds to qb<->q, Qbar<->Q, c.f. Eqs. (14) and (15)
      if (hq == 2) then
         q=1
         QQ=2
         qb=3
         QQb=4
         g=5
      else
         q=3
         QQ=4
         qb=1
         QQb=2
         g=5
      endif
      P=QQ
      Pb=QQb
      pntr_r = [q, qb]
      pntr_RR = [QQ, QQb]

! Eq. (31)
      HQQ_QQb(1)=
     & xn*(FF(qb,g,q,s)+FF(P,q,g,s)-FF(Pb,g,qb,s)
     &    -FF(P,g,q,s)-FF(P,q,qb,s)-FF(g,q,qb,s)
     &  +h(qb,g,Pb,P)*FF(g,qb,Pb,s)-h(qb,q,Pb,P)*FF(q,qb,Pb,s)
     &  -h(qb,g,q,P)*FF(g,qb,q,s))
     & -one/xn*(FF(qb,q,P,s)-FF(qb,q,Pb,s)-FF(P,qb,q,s)
     &  +FF(qb,Pb,g,s)-FF(P,Pb,g,s)
     &  -FF(qb,P,g,s)-FF(q,Pb,g,s)+h(qb,q,Pb,P)*FF(q,qb,Pb,s)
     &  +h(qb,q,g,P)*FF(q,P,g,s)-h(qb,Pb,g,P)*FF(Pb,P,g,s))
! Eq. (32)
      HQQ_QQb(2)=
     & xn*(FF(qb,g,q,s)+FF(P,q,g,s)-FF(Pb,g,qb,s)-FF(P,g,q,s)
     &  -FF(P,q,qb,s)-FF(g,q,qb,s)
     &  +FF(g,qb,Pb,s)-FF(q,qb,Pb,s)-h(qb,g,q,Pb)*FF(g,qb,q,s))
     & -one/xn*(FF(qb,q,P,s)-FF(qb,q,Pb,s)+FF(Pb,qb,q,s)+FF(qb,Pb,g,s)
     &  +FF(q,P,g,s)-FF(qb,P,g,s)-FF(Pb,P,g,s)
     &  -h(qb,q,P,Pb)*FF(P,qb,q,s)-h(qb,q,g,Pb)*FF(q,Pb,g,s)
     &  -h(qb,P,g,Pb)*FF(P,Pb,g,s))
! Eq. (33)
      do hQQ=1,2
      RR=pntr_RR(hQQ)
      HQQ_qb(hQQ)=
     & xn*(FF(qb,g,Pb,s)-FF(qb,g,q,s)+FF(P,q,qb,s)
     &  -FF(P,q,g,s)-FF(P,Pb,qb,s)
     &  -h(qb,g,Pb,RR)*FF(g,qb,Pb,s)
     &  +h(qb,g,q,RR)*FF(g,qb,q,s)
     &  +h(qb,g,P,RR)*FF(g,Pb,P,s))
      Hq_QQb(hQQ)=
     & xn*(FF(q,g,P,s)-FF(q,g,qb,s)-FF(P,q,g,s)+FF(qb,q,g,s)
     &  +h(qb,q,Pb,RR)*FF(q,qb,Pb,s)-h(qb,g,Pb,RR)*FF(g,qb,Pb,s)
     &  +h(qb,g,Pb,RR)*FF(g,P,Pb,s)
     &  -h(qb,q,Pb,RR)*FF(q,P,Pb,s)+h(qb,g,q,RR)*FF(g,qb,q,s)
     &  -h(qb,q,g,RR)*FF(q,qb,g,s))
      enddo

! explicit expressions for tree-level(+,hQQ), so that
! interchange of labels for hq=1 are properly applied
      aQQ_QQb0_plus(1)=za(qb,P)**2/(za(q,qb)*za(P,Pb))
     &             *za(P,Pb)/(za(P,g)*za(g,Pb))
      aQQ_qb0_plus(1)=za(qb,P)**2/(za(q,qb)*za(P,Pb))
     &             *za(P,qb)/(za(P,g)*za(g,qb))
      aq_QQb0_plus(1)=za(P,qb)**2/(za(P,Pb)*za(q,qb))
     &             *za(q,Pb)/(za(q,g)*za(g,Pb))

      aQQ_QQb0_plus(2)=-za(qb,Pb)**2/(za(q,qb)*za(P,Pb))
     &             *za(P,Pb)/(za(P,g)*za(g,Pb))
      aQQ_qb0_plus(2)=-za(qb,Pb)**2/(za(q,qb)*za(P,Pb))
     &             *za(P,qb)/(za(P,g)*za(g,qb))
      aq_QQb0_plus(2)=-za(Pb,qb)**2/(za(P,Pb)*za(q,qb))
     &             *za(q,Pb)/(za(q,g)*za(g,Pb))
     
!      if (hq == 2) then
!        write(6,*) 'aQQ_QQb0(2,1)',aQQ_QQb0(2,1)/aQQ_QQb0_plus(1)
!        write(6,*) 'aq_QQb0(2,1)',aq_QQb0(2,1)/aq_QQb0_plus(1)
!        write(6,*) 'aQQ_qb0(2,1)',aQQ_qb0(2,1)/aQQ_qb0_plus(1)
!        write(6,*) 'aQQ_QQb0(2,2)',aQQ_QQb0(2,2)/aQQ_QQb0_plus(2)
!        write(6,*) 'aq_QQb0(2,2)',aq_QQb0(2,2)/aq_QQb0_plus(2)
!        write(6,*) 'aQQ_qb0(2,2)',aQQ_qb0(2,2)/aQQ_qb0_plus(2)
!        pause
!      endif

! assembly per Eq. (30)
      do hQQ=1,2
      if (hq == 2) then
        EQQ_QQb(hq,hQQ)=
     &    aQQ_QQb0_plus(hQQ)*HQQ_QQb(hQQ)
     &   +aQQ_qb0_plus(hQQ)*HQQ_qb(hQQ)
     &   +aq_QQb0_plus(hQQ)*Hq_QQb(hQQ)
      else
        EQQ_QQb(hq,3-hQQ)=-(
     &    aQQ_QQb0_plus(hQQ)*HQQ_QQb(hQQ)
     &   +aQQ_qb0_plus(hQQ)*HQQ_qb(hQQ)
     &   +aq_QQb0_plus(hQQ)*Hq_QQb(hQQ))
      endif
      enddo
      
      enddo

      return
      end


      function FF(i,j,k,s)
! Eq. (24)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      integer i,j,k,m,n,check(5),r
      complex(dp) :: FF,lnrat,ddilog
      real(dp) :: s(mxpart,mxpart)

! identift complementary labels (m.n) according to Eq. (25)
      check(:)=0
      check(i)=1
      check(j)=1
      check(k)=1
      
      do r=1,4
        if (check(r) == 0) then
          m=r
          exit
         endif
      enddo
      n=15-i-j-k-m
      
      FF=-ddilog(one-s(m,n)/s(i,j))-ddilog(one-s(m,n)/s(j,k))
     & -half*lnrat(-s(i,j),-s(j,k))**2-pisqo6

      return
      end


! Kronecker delta(i,j)
      function krondel(i,j)
      implicit none
      integer krondel,i,j
      
      if (i==j) then
        krondel=1
      else
        krondel=0
      endif
      
      return
      end

      

