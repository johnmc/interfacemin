      subroutine fillRCij(za,zb,s,aQQ_qb0,aQQ_QQb0,
     & RQQ_qb,RQQ_QQb,CQQ_QQb,CQQ_qb)
!--- renormalization and collinear contributions from 9405386, Eqs. (11)-(13)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'scale.f'
      include 'sprods_decl.f'
      include 'zprods_decl.f'
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
      integer, parameter :: q=1, QQ=2, qb=3, QQb=4, g=5
! definitions of helicity-dependent momenta , Eq. (5)
      integer, parameter, dimension(2) :: r = [q, qb]
      integer, parameter, dimension(2) :: RR = [QQ, QQb]
      complex(dp) :: aQQ_qb0(2,2),aQQ_QQb0(2,2),epinvPij,lnrat,fac
      complex(dp) :: RQQ_qb(2,2),RQQ_QQb(2,2),CQQ_QQb(2,2),CQQ_qb(2,2)
      integer hq,hQQ,i,j

! statement function for 1/e*Pij
      epinvPij(i,j)=epinv+lnrat(musq,-s(i,j))

! Eq. (11)
      fac=-half*epinv*(11*xn-2*dfloat(nflav))
      RQQ_qb(:,:)=fac*aQQ_qb0(:,:)
      RQQ_QQb(:,:)=fac*aQQ_QQb0(:,:)
      
! Eq. (12)
      fac=two/three*(xn-float(nflav))*epinvPij(qb,q)
     & +three/two/xn*(epinvPij(qb,q)+epinvPij(QQb,QQ))
     & +29._dp/18._dp*xn+13._dp/2._dp/xn-10._dp/9._dp*float(nflav)
      CQQ_QQb(:,:)=fac*aQQ_QQb0(:,:)

! Eq. (13)
      fac=two/three*(xn-float(nflav))*epinvPij(QQb,QQ)
     & +three/two/xn*(epinvPij(qb,q)+epinvPij(QQb,QQ))
     & +29._dp/18._dp*xn+13._dp/2._dp/xn-10._dp/9._dp*float(nflav)
      CQQ_qb(:,:)=fac*aQQ_qb0(:,:)

      return
      end
