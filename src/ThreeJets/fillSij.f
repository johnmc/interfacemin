      subroutine fillSij(za,zb,s,aQQ_qb0,aQQ_QQb0,aq_qb0,aq_QQb0,SQQ_qb,SQQ_QQb)
!--- soft contributions from 9405386, Eqs. (9) and (10)
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'epinv.f'
      include 'epinv2.f'
      include 'mxpart.f'
      include 'scale.f'
      include 'sprods_decl.f'
      include 'zprods_decl.f'
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
      integer, parameter :: q=1, QQ=2, qb=3, QQb=4, g=5
! definitions of helicity-dependent momenta , Eq. (5)
      integer, parameter, dimension(2) :: r = [q, qb]
      integer, parameter, dimension(2) :: RR = [QQ, QQb]
      complex(dp) :: aQQ_qb0(2,2),aQQ_QQb0(2,2),aq_qb0(2,2),aq_QQb0(2,2)
      complex(dp) :: SQQ_qb(2,2),SQQ_QQb(2,2),epinv2Pij,lnrat
      integer i,j

! statement function for 1/e^2*Pij
      epinv2Pij(i,j)=
     & epinv*epinv2+epinv*lnrat(musq,-s(i,j))+half*lnrat(musq,-s(i,j))**2

! Eq. (9)
      SQQ_qb(:,:)=-(
     & xn*aQQ_qb0(:,:)*(epinv2Pij(qb,g)+epinv2Pij(QQb,q)+epinv2Pij(QQ,g))
     & -one/xn*aQQ_qb0(:,:)*(
     &    -epinv2Pij(qb,QQb)+epinv2Pij(qb,QQ)+epinv2Pij(qb,q)
     &    +epinv2Pij(QQb,QQ)+epinv2Pij(QQb,q)-epinv2Pij(QQ,q))
     & -one/xn*aQQ_QQb0(:,:)*(-epinv2Pij(qb,QQb)+epinv2Pij(qb,g)
     &                        +epinv2Pij(QQb,q)-epinv2Pij(q,g))
     & -one/xn*aq_qb0(:,:)*(epinv2Pij(QQb,q)-epinv2Pij(QQb,g)
     &                     -epinv2Pij(QQ,q)+epinv2Pij(QQ,g)))
     
! Eq. (10)
      SQQ_QQb(:,:)=-(
     & xn*aQQ_QQb0(:,:)*(epinv2Pij(qb,q)+epinv2Pij(QQb,g)+epinv2Pij(QQ,g))
     & -one/xn*aQQ_QQb0(:,:)*(-epinv2Pij(qb,QQb)+epinv2Pij(qb,QQ)+epinv2Pij(qb,q)
     &                        +epinv2Pij(QQb,QQ)+epinv2Pij(QQb,q)-epinv2Pij(QQ,q))
     & +xn*aQQ_qb0(:,:)*(epinv2Pij(qb,QQb)-epinv2Pij(qb,q)
     &                  -epinv2Pij(QQb,g)+epinv2Pij(q,g))
     & +xn*aq_QQb0(:,:)*(-epinv2Pij(qb,q)+epinv2Pij(qb,g)
     &                   +epinv2Pij(QQ,q)-epinv2Pij(QQ,g)))

      return
      end
