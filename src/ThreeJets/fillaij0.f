      subroutine fillaij0(za,zb,aQQ_qb0,aQQ_QQb0)
!--- tree-level amplitude from 9405386, Eq. (6)
!--- multiplied by a factor (-i)
      implicit none
      include 'types.f'
      include 'mxpart.f'
      include 'zprods_decl.f'
! pointers, noting that the notation of the paper is translated as follows:
!  q -> q, Q -> QQ, qbar -> qb, Qbar -> QQb
      integer, parameter :: q=1, QQ=2, qb=3, QQb=4, g=5
! definitions of helicity-dependent momenta , Eq. (5)
      integer, parameter, dimension(2) :: r = [q, qb]
      integer, parameter, dimension(2) :: RR = [QQ, QQb]
      complex(dp) :: pa, aQQ_qb0(2,2),aQQ_QQb0(2,2)
      integer hq,hQQ
      
      do hq=1,2
      do hQQ=1,2
      pa=za(r(hq),RR(hQQ))**2/(za(q,qb)*za(QQ,QQb))
      if (hq == hQQ) pa=-pa
      aQQ_qb0(hq,hQQ)=pa*za(QQ,qb)/(za(QQ,g)*za(g,qb))
      aQQ_QQb0(hq,hQQ)=pa*za(QQ,QQb)/(za(QQ,g)*za(g,QQb))
      enddo
      enddo

      return
      end
