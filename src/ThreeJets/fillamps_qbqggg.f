      subroutine fillamps_qbqggg(j1,j2,j3,j4,j5,
     &  miA5tree_QBQggg,AL_QBQggg,ASUSY_QBQggg,Af_QBQggg,As_QBQggg,
     &  miA5tree_QBgQgg,AL_QBgQgg,ASUSY_QBgQgg,Af_QBgQgg,As_QBgQgg)
! computes primiitive amplitudes for all helicity combinations
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'mxpart.f'
      include 'zprods_com.f'
      integer j1,j2,j3,j4,j5
      complex(dp) :: miA5tree,AL,ASUSY,Af,As
! helicity amplitudes are labelled by (qb, g, g, g)
      complex(dp) :: miA5tree_QBQggg(2,2,2,2),AL_QBQggg(2,2,2,2),
     & ASUSY_QBQggg(2,2,2,2),Af_QBQggg(2,2,2,2),As_QBQggg(2,2,2,2)
      complex(dp) :: miA5tree_QBgQgg(2,2,2,2),AL_QBgQgg(2,2,2,2),
     & ASUSY_QBgQgg(2,2,2,2),Af_QBgQgg(2,2,2,2),As_QBgQgg(2,2,2,2)

! note that this should fill contributions
! QB(1) Q(2) g(3) g(4) g(5)
! QB(1) g(3) Q(2) g(4) g(5)
! hence calls to QBQggg are (1,2,3,4,5) and (2,1,3,4,5)
!   and calls to QBgQgg are (1,3,2,4,5) and (2,3,1,4,5)

! only fill contributions that enter at NLO
! i.e. amplitudes that are zero at tree-level and finite at 1-loop
! are omitted, i.e. g+ g+ g+, g- g- g-
      miA5tree_QBQggg(:,:,:,:)=czip
      AL_QBQggg(:,:,:,:)=czip
      ASUSY_QBQggg(:,:,:,:)=czip
      Af_QBQggg(:,:,:,:)=czip
      As_QBQggg(:,:,:,:)=czip
      miA5tree_QBgQgg(:,:,:,:)=czip
      AL_QBgQgg(:,:,:,:)=czip
      ASUSY_QBgQgg(:,:,:,:)=czip
      Af_QBgQgg(:,:,:,:)=czip
      As_QBgQgg(:,:,:,:)=czip

! six basic amplitudes (three helicities, qb-q-g-g-g and qb-g-q-g-g)
      call A5qbmqpgmgpgp(j1,j2,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(1,1,2,2)=miA5tree
      AL_QBQggg(1,1,2,2)=AL
      ASUSY_QBQggg(1,1,2,2)=ASUSY
      Af_QBQggg(1,1,2,2)=Af
      As_QBQggg(1,1,2,2)=As

      call A5qbmgmqpgpgp(j1,j3,j2,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(1,1,2,2)=miA5tree
      AL_QBgQgg(1,1,2,2)=AL
      ASUSY_QBgQgg(1,1,2,2)=ASUSY
      Af_QBgQgg(1,1,2,2)=Af
      As_QBgQgg(1,1,2,2)=As
      
      call A5qbmqpgpgmgp(j1,j2,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(1,2,1,2)=miA5tree
      AL_QBQggg(1,2,1,2)=AL
      ASUSY_QBQggg(1,2,1,2)=ASUSY
      Af_QBQggg(1,2,1,2)=Af
      As_QBQggg(1,2,1,2)=As

      call A5qbmgpqpgmgp(j1,j3,j2,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(1,2,1,2)=miA5tree
      AL_QBgQgg(1,2,1,2)=AL
      ASUSY_QBgQgg(1,2,1,2)=ASUSY
      Af_QBgQgg(1,2,1,2)=Af
      As_QBgQgg(1,2,1,2)=As

      call A5qbmqpgpgpgm(j1,j2,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(1,2,2,1)=miA5tree
      AL_QBQggg(1,2,2,1)=AL
      ASUSY_QBQggg(1,2,2,1)=ASUSY
      Af_QBQggg(1,2,2,1)=Af
      As_QBQggg(1,2,2,1)=As

      call A5qbmgpqpgpgm(j1,j3,j2,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(1,2,2,1)=miA5tree
      AL_QBgQgg(1,2,2,1)=AL
      ASUSY_QBgQgg(1,2,2,1)=ASUSY
      Af_QBgQgg(1,2,2,1)=Af
      As_QBgQgg(1,2,2,1)=As

! complex conjugation (parity)
      call A5qbmqpgmgpgp(j1,j2,j3,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(2,2,1,1)=-miA5tree
      AL_QBQggg(2,2,1,1)=-AL
      ASUSY_QBQggg(2,2,1,1)=-ASUSY
      Af_QBQggg(2,2,1,1)=-Af
      As_QBQggg(2,2,1,1)=-As

      call A5qbmgmqpgpgp(j1,j3,j2,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(2,2,1,1)=-miA5tree
      AL_QBgQgg(2,2,1,1)=-AL
      ASUSY_QBgQgg(2,2,1,1)=-ASUSY
      Af_QBgQgg(2,2,1,1)=-Af
      As_QBgQgg(2,2,1,1)=-As
      
      call A5qbmqpgpgmgp(j1,j2,j3,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(2,1,2,1)=-miA5tree
      AL_QBQggg(2,1,2,1)=-AL
      ASUSY_QBQggg(2,1,2,1)=-ASUSY
      Af_QBQggg(2,1,2,1)=-Af
      As_QBQggg(2,1,2,1)=-As

      call A5qbmgpqpgmgp(j1,j3,j2,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(2,1,2,1)=-miA5tree
      AL_QBgQgg(2,1,2,1)=-AL
      ASUSY_QBgQgg(2,1,2,1)=-ASUSY
      Af_QBgQgg(2,1,2,1)=-Af
      As_QBgQgg(2,1,2,1)=-As

      call A5qbmqpgpgpgm(j1,j2,j3,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(2,1,1,2)=-miA5tree
      AL_QBQggg(2,1,1,2)=-AL
      ASUSY_QBQggg(2,1,1,2)=-ASUSY
      Af_QBQggg(2,1,1,2)=-Af
      As_QBQggg(2,1,1,2)=-As

      call A5qbmgpqpgpgm(j1,j3,j2,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(2,1,1,2)=-miA5tree
      AL_QBgQgg(2,1,1,2)=-AL
      ASUSY_QBgQgg(2,1,1,2)=-ASUSY
      Af_QBgQgg(2,1,1,2)=-Af
      As_QBgQgg(2,1,1,2)=-As

! charge conjugation
      call A5qbmqpgmgpgp(j2,j1,j3,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(1,2,1,1)=miA5tree
      AL_QBQggg(1,2,1,1)=AL
      ASUSY_QBQggg(1,2,1,1)=ASUSY
      Af_QBQggg(1,2,1,1)=Af
      As_QBQggg(1,2,1,1)=As

      call A5qbmgmqpgpgp(j2,j3,j1,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(1,2,1,1)=miA5tree
      AL_QBgQgg(1,2,1,1)=AL
      ASUSY_QBgQgg(1,2,1,1)=ASUSY
      Af_QBgQgg(1,2,1,1)=Af
      As_QBgQgg(1,2,1,1)=As
      
      call A5qbmqpgpgmgp(j2,j1,j3,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(1,1,2,1)=miA5tree
      AL_QBQggg(1,1,2,1)=AL
      ASUSY_QBQggg(1,1,2,1)=ASUSY
      Af_QBQggg(1,1,2,1)=Af
      As_QBQggg(1,1,2,1)=As

      call A5qbmgpqpgmgp(j2,j3,j1,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(1,1,2,1)=miA5tree
      AL_QBgQgg(1,1,2,1)=AL
      ASUSY_QBgQgg(1,1,2,1)=ASUSY
      Af_QBgQgg(1,1,2,1)=Af
      As_QBgQgg(1,1,2,1)=As

      call A5qbmqpgpgpgm(j2,j1,j3,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(1,1,1,2)=miA5tree
      AL_QBQggg(1,1,1,2)=AL
      ASUSY_QBQggg(1,1,1,2)=ASUSY
      Af_QBQggg(1,1,1,2)=Af
      As_QBQggg(1,1,1,2)=As

      call A5qbmgpqpgpgm(j2,j3,j1,j4,j5,zb,za,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(1,1,1,2)=miA5tree
      AL_QBgQgg(1,1,1,2)=AL
      ASUSY_QBgQgg(1,1,1,2)=ASUSY
      Af_QBgQgg(1,1,1,2)=Af
      As_QBgQgg(1,1,1,2)=As

! complex conjugation (parity) + charge conjugation
      call A5qbmqpgmgpgp(j2,j1,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(2,1,2,2)=-miA5tree
      AL_QBQggg(2,1,2,2)=-AL
      ASUSY_QBQggg(2,1,2,2)=-ASUSY
      Af_QBQggg(2,1,2,2)=-Af
      As_QBQggg(2,1,2,2)=-As

      call A5qbmgmqpgpgp(j2,j3,j1,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(2,1,2,2)=-miA5tree
      AL_QBgQgg(2,1,2,2)=-AL
      ASUSY_QBgQgg(2,1,2,2)=-ASUSY
      Af_QBgQgg(2,1,2,2)=-Af
      As_QBgQgg(2,1,2,2)=-As
      
      call A5qbmqpgpgmgp(j2,j1,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(2,2,1,2)=-miA5tree
      AL_QBQggg(2,2,1,2)=-AL
      ASUSY_QBQggg(2,2,1,2)=-ASUSY
      Af_QBQggg(2,2,1,2)=-Af
      As_QBQggg(2,2,1,2)=-As

      call A5qbmgpqpgmgp(j2,j3,j1,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(2,2,1,2)=-miA5tree
      AL_QBgQgg(2,2,1,2)=-AL
      ASUSY_QBgQgg(2,2,1,2)=-ASUSY
      Af_QBgQgg(2,2,1,2)=-Af
      As_QBgQgg(2,2,1,2)=-As

      call A5qbmqpgpgpgm(j2,j1,j3,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBQggg(2,2,2,1)=-miA5tree
      AL_QBQggg(2,2,2,1)=-AL
      ASUSY_QBQggg(2,2,2,1)=-ASUSY
      Af_QBQggg(2,2,2,1)=-Af
      As_QBQggg(2,2,2,1)=-As

      call A5qbmgpqpgpgm(j2,j3,j1,j4,j5,za,zb,miA5tree,AL,ASUSY,Af,As)
      miA5tree_QBgQgg(2,2,2,1)=-miA5tree
      AL_QBgQgg(2,2,2,1)=-AL
      ASUSY_QBgQgg(2,2,2,1)=-ASUSY
      Af_QBgQgg(2,2,2,1)=-Af
      As_QBgQgg(2,2,2,1)=-As

      return
      end


