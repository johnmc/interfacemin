!
!  SPDX-License-Identifier: GPL-3.0-or-later
!  Copyright (C) 2019-2022, respective authors of MCFM.
!

      subroutine qqb_thrjet_v(p,msq)
c*******************************************************************************
c                                                                              *
c   Author: J. Campbell, September 2014                                        *
c   Virtual matrix elements for three-jet production                             *
c                                                                              *
c*******************************************************************************
      implicit none
      include 'types.f'
      include 'constants.f'
      include 'nf.f'
      include 'mxpart.f'
      include 'nflav.f'
      include 'qcdcouple.f'
      include 'zprods_com.f'
      include 'scheme.f'
      include 'blha.f'
      integer j,k
      real(dp):: msq(-nf:nf,-nf:nf),p(mxpart,4),fac,
     &  A5NLOggggg,gg,qqb,qbq,qg,qbg,gq,gqb,A5NLOqbqggg,
     & qrqr,qqqq,A5NLO4qg

      scheme='tH-V'
      msq(:,:)=zip

      call spinoru(5,p,za,zb)

! include final-state averaging factors here
! gg -> ggg
      if (useblha == 0 .or. (blhatype == 1)) then
        gg=A5NLOggggg()/6._dp
      else
        gg=zip
      endif

      
! gg -> qbqg
      if (useblha == 0) then
        gg=gg+float(nflav)*A5NLOqbqggg(3,4,1,2,5)
      endif
! qqb -> ggg
      if (useblha == 0 .or. (blhatype == 2)) then
        qqb=A5NLOqbqggg(1,2,3,4,5)/6._dp
      else
        qqb=zip
      endif
! qbq -> ggg
      if (useblha == 0) then
        qbq=A5NLOqbqggg(2,1,3,4,5)/6._dp
      else
        qbq=zip
      endif
! qg -> ggq
      if (useblha == 0) then
        qg=A5NLOqbqggg(1,5,3,4,2)/2._dp
      else
        qg=zip
      endif
! gqb -> ggqb
      if (useblha == 0) then
        gqb=A5NLOqbqggg(5,2,3,4,1)/2._dp
      else
        gqb=zip
      endif
! gq -> ggq
      if (useblha == 0) then
        gq=A5NLOqbqggg(2,5,3,4,1)/2._dp
      else
        gq=zip
      endif
! qbg -> ggqb
      if (useblha == 0) then
        qbg=A5NLOqbqggg(5,1,3,4,2)/2._dp
      else
        qbg=zip
      endif

! This 4-quark section should be extended if we really want to compute
! all 4-quark contributions (not just relevant ones for the interface)
! q r -> q r g
      if ((useblha == 0) .or. (blhatype == 4)) then
        qrqr=A5NLO4qg(1,2,3,4,5,.false.)
      endif
      if ((useblha == 0) .or. (blhatype == 3)) then
        qqqq=A5NLO4qg(1,2,3,4,5,.true.)
      endif

! fill array, including initial state averaging
      msq(0,0) = avegg*gg
      do j=1,nf
      msq(j,-j)=aveqq*qqb
      msq(-j,j)=aveqq*qbq
      msq(j,0)=aveqg*qg
      msq(-j,0)=aveqg*qbg
      msq(0,j)=aveqg*gq
      msq(0,-j)=aveqg*gqb
      enddo
      
      do j=1,nf
      do k=1,nf
      if (j == k) then
        msq(j,k) = aveqq*qqqq
      else
        msq(j,k) = aveqq*qrqr
      endif
      enddo
      enddo

      return
      end
